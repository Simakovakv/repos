/*----global variables for list-page queries----*/
count=10;
initial_num_act=0;
initial_num_all=0;
    
/*-----functions to get list from DB on Active or All tabs---------*/
    
function getTableData() {
        getTableDataActive(initial_num_act);
        getTableDataAll(initial_num_all);
}

function getTableDataActive(start) {
    $.getJSON('/app/list?status=active&start='+start+'&count='+count).success(function(data) {
         if (data.length===0) 
            document.getElementById('content-load-act').style.display = 'none'; 
         else
            drawTable(data, "active");
    });
}

function getTableDataAll(start) {
    $.getJSON('/app/list?&start='+start+'&count='+count).success(function(data) {
        if (data.length===0) {
            document.getElementById('content-load-all').style.display = 'none'; 
        }
        else
            drawTable(data, "all");
    });
}
          
function drawTable(data, key) {
    for (var i = 0; i < data.length; i++) {
        drawRow(data[i], key);
    }
}

function drawRow(rowData, key) {
        if (key==="all") 
            makeRow($("#psrListTableAll"));    
        if (key === "active"){
            makeRow($("#psrListTableActive"));
        }
        function makeRow(parentElement) {
            var status_row=translateStatus(rowData.status);
            var row = $('<tr />');
            row.append($('<td class="element">' + rowData.start_date + '</td>'));
            row.append($('<td class="element">' + "ПСР № " + rowData.sar_num + '</td>'));
            row.append($('<td class="element">' + rowData.short_desc + '</td>'));
            row.append($('<td class="element">' + status_row + '</td>'));
            row.click(function (){
                window.location.href = 'http://localhost/info.html?id=' + rowData.sar_num;
            });
            parentElement.append(row);
        }
}
/*----function to get more strings on list with button click-----*/
function getMoreRowAll() {
        initial_num_all+=count;
        getTableDataAll(initial_num_all);
}

function getMoreRowActive() {
        initial_num_act+=count;
        getTableDataActive(initial_num_act);
}
/*-----functions to get info from DB on info page---------*/
function getSetSarInfo (){
    var id = getQueryVariable('id');
    if($("#id").is(':empty')) $("#id").append(id);
     
    $.getJSON('/app/info?id='+ id).success(function(data) {
        var status_row=translateStatus(data.status);
        if($("#status_op").is(':empty')) $("#status_op").append(status_row);  
        if($("#full_name").is(':empty')) org_name = $("#full_name").append(data.org_name);
        if($("#start_date").is(':empty')) start_date = $("#start_date").append(data.start_date);            
        if($("#firstOpenInfo").is(':empty')) $("#firstOpenInfo").append(data.general_info);
        if($("#firstCloseInfo").is(':empty')) $("#firstCloseInfo").append(data.private_info);
        if($("#secondCloseInfo").is(':empty')) $("#secondCloseInfo").append(data.secondary_info);
    });
}
function getInfoData() {
    
    getSetSarInfo ();
    getMapLink();
     
            
}
function getMapLink() {
    var id = getQueryVariable('id');
    $("#get_geo_data").click(function (){
                window.location.href = 'http://localhost/map.html?id=' + id;
            });
}

/*----common function to get variable from address bar-----*/
function getQueryVariable(variable) {
    var query = document.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
}
/*______Add auth part__________(using rel=import) */
//function getAuth(){
//    var link = document.querySelector('link[rel=import]');
//    var content = link.import.querySelector('#header');
//    document.body.appendChild(content.cloneNode(true));
//}
function translateStatus(str) {
    var trans=new Array();
    trans['active']='Активные';     
    trans['closed']='Завершены';
    trans['waiting']='Ожидание';
    trans['monitoring']='Мониторинг';
    trans['invalid']='Ошибка';
    if(!trans.str) 
        return trans[str];
     else return Error;
    }

function retranslateStatus(str) {
    var retrans=new Array();
    retrans['Активные']='active';     
    retrans['Завершены']='closed';
    retrans['Ожидание']='waiting';
    retrans['Мониторинг']='monitoring';
    retrans['Ошибка']='invalid';
    if(!retrans.str) 
        return retrans[str];
     else return Error;
    }
/*--------functions to get reminders from DB--------*/

//function getListReminders() {
//    var id = getQueryVariable('id');
//    console.log(id);
//    $.getJSON('/app/reminders?id='+ id).success(function(data) {
//        for (var i = 0; i < data.length; i++) {
//            getReminder(data[i]);
//    }
//});
//}
//function getReminder(data) {
//    makeRem($("#edit_rem"));
//    function makeRem(pElement) {
//            var rem = $('<tr />');
//            rem.append($('<td class="element">' + data.alarm + '</td>'));
//            rem.append($('<td class="element">' + + data.descr + '</td>'));
//            rem.append($('<td class="element">' + data.create_date + '</td>'));
//            
//        pElement.append(rem);
//        }
//
//}
//        
//edit and add forms functions
function addForm () {
    var lat = 30.5;
    var long = 60.5;
        $("#form-add").submit(function(event) {
            event.preventDefault();
            $.get("/app/org?name=" + $('#org_name').val()).success(function(data) {
                console.log(data);
                $.post("/app/info", {'org':data, 'descr' : $('#short_desc').val(), 'lat' : lat, 'long' : long },
//            $.post("/app/info", $("#form")serialize(),    
            function(data) {
                $("#org_name").val('');   
                $("#short_desc").val('');
                alert("Данные были успешно добавлены!");
//                $(".modal").css( "display", "none" );
                location.reload();
            }).error(function(){
                alert("Error on connection with server");
            });
                
            }).error(function(){
                alert("Такой организации не существует. Попробуйте снова");
            });
        });
    }

function editInfo() {
   
    var id_var = getQueryVariable('id');
    $.getJSON('/app/info?id='+ id_var).success(function(data) {
        $("#short_desc_edit").append(data.short_desc);
        $("#statusEdit option[value=" + data.status + "]").prop("selected",true);
//        $("#statusEdit").append(data.status);  
        $("#firstOpenInfoEdit").append(data.general_info);
        $("#firstCloseInfoEdit").append(data.private_info);
        $("#secondCloseInfoEdit").append(data.secondary_info);
        var short_desc = data.short_desc;
        var date=data.start_date;
        var org = data.org_name;
        var lat = data.lat;
        var long = data.long;
        console.log(data);
      
        $("#form-edit").submit(function(event) {
            event.preventDefault();
            var ginfo=$("#firstOpenInfoEdit").val();
            var pinfo=$("#firstCloseInfoEdit").val();
            var sinfo=$("#secondCloseInfoEdit").val();
            var short_desc =$("#short_desc_edit").val();
            var status=$("#statusEdit").val();
            if (pinfo === '') pinfo = "null";
            if(sinfo === '') sinfo = "null";
            $.ajax({
                type: 'PUT',
                dataType: 'json', 
                url: "/app/info", 
                data: {id: id_var, ginfo: ginfo, pinfo: pinfo, sinfo: sinfo , descr: short_desc, lat : lat, long : long, date: date, status: status },
                complete: function() {
                    
                    alert("Данные были успешно добавлены!");
                    $(".modal-wrap").hide();
                    window.location.reload();
                    $("#auth").update();
                }
        });
    });
    });
}
function getAuthLine () {
        $(document).ready(function() {
            if ($.cookie('uid')===null)
                $("#header").load("main_page_parts/auth.html");
            else {
                $.cookie('uid', null);
                $("#header").load("main_page_parts/auth.html");
                checkUser($.cookie('user'), param());
                
        
//        getUser("/app/login", $.cookie('user'), $.cookie('password'));
//            location.update();
            
        };
    });
    console.log($.cookie('uid'));
}
function param () {
            return ["/app/login", $.cookie('user'), $.cookie('password')];
        }
function getAuthInfo () {
    var $form = $("#auth-form"),
            user = $form.find( "input[name='user']" ).val(),
            url = $form.attr( "action" ),
            
            shaObj = new jsSHA("SHA-512", "TEXT");
            shaObj.update(document.getElementById("pwd").value);
            var hash = shaObj.getHash("HEX");
//            document.getElementById("pwd").value = hash;
            document.cookie = "password=" +hash;
            return [url, user, hash];
}
function autentification (){
        $("#auth").submit(function (event) {
            event.preventDefault();
            checkUser(document.getElementById("user").value, getAuthInfo());
            location.reload();
        });

}
function checkUser(user_value, param) {
    if (user_value===null) return;
    else {
        userIsExist(user_value);
         getUser(param);
    }


}
function userIsExist (user) {
    $.get('/app/login?user=' + user)
                .success(function (data) {
                })
                .error(function (data) {
                    alert("Такого пользователя не существует");
                });
}

function getUser ([url, user, pwd]) {
     var posting = $.post( url, { user: user, password: pwd} );

            posting.done( function (data) {
//                switchAuthLine();
                $('#auth').hide();
                $("#intro_auth").show();
                $('#intro_user' ).empty().append( data );
                accessPremissions();
//                 $("#auth").html(data); 
                });

            }

//function switchAuthLine () {
//            $('#auth').toggle();
//            $("#intro_auth").toggle();
//            accessPremissions();
//    
//}
function  logout (){
    $("#intro_auth_form").submit(function(e) {        
            e.preventDefault();
            $.get('/app/logout');
            $.cookie('uid', null);
            $.cookie('user', null);
            $.cookie('password', null);
            $('#auth').show();
            $('#intro_auth').hide();
            accessPremissions();
        });
    location.reload();
            
//            location.reload();
//            switchAuthLine();
}

function accessPremissions() {
    var url = document.location.href;
    var matchList = "index"; 
    var matchInfo = "info"; 
    var matchMap = "map";
    if ($.cookie('user')==="user2"||$.cookie('user')==="user3") {
        if (url.match(matchList))
            $('#openAddForm').show(); 
        else if (url.match(matchInfo)) {
            $('#openEditForm').show();
            $('#authentication-required').show(); 
        }
//         else if (url.match(matchMap)) {
//             $("#radio_area").load("map-page-parts/radio.html");
//             $("#properties_objects").load("map-page-parts/control_tools.html");
//             $("#radio_area").show();
// //            $("#properties_objects").show();
//             $("#hideMapBtn").show();
//         }
        
    }
    else if ($.cookie('user')==="user1") {
        if (url.match(matchInfo)) 
            $('#authentication-required').show();
        // else if (url.match(matchMap)) {
        // $("#radio_area").load("map-page-parts/radio.html");
        // $("#radio_area").show();
        // }
    }
    else {
        if (url.match(matchList))
            $('#openAddForm').hide(); 
        if (url.match(matchInfo)) {
            $('#openEditForm').hide();
            $('#authentication-required').hide(); }
       //  else if (url.match(matchMap)) {
       //      $("#hideMapBtn").hide();
       //      $("#radio_area").hide();}
       // $("#properties_objects").hide();
 
    }
    getSetSarInfo();


}