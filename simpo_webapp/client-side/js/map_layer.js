/**
 * Map layer rendering
 */

var AMapPage = function () {
};
var MapPage = new AMapPage();

/**
 *This method takes google overlays from googleMutant
 *@return: overlays
 */
AMapPage.prototype.getGoogleOverlays = function () {
    var overlays = {
        "Google Labels": new L.gridLayer.googleMutant({
            styles: [
                {featureType: 'water', stylers: [{visibility: 'off'}]},
                {featureType: 'landscape', stylers: [{visibility: 'off'}]},
                {elementType: 'labels', stylers: [{visibility: 'on'}]},
                {featureType: 'road', stylers: [{visibility: 'off'}]},
                {featureType: 'poi', stylers: [{visibility: 'off'}]},
                {featureType: 'transit', stylers: [{visibility: 'off'}]},
                {featureType: 'administrative', stylers: [{visibility: 'on'}]},
                {featureType: 'administrative.locality', stylers: [{visibility: 'on'}]}
            ],
            type: 'roadmap'
        }),
        "Google Roads": new L.gridLayer.googleMutant({
            styles: [
                {featureType: 'water', stylers: [{visibility: 'off'}]},
                {featureType: 'landscape', stylers: [{visibility: 'off'}]},
                {elementType: 'labels', stylers: [{visibility: 'off'}]},
                {featureType: 'road', stylers: [{visibility: 'on'}]},
                {featureType: 'poi', stylers: [{visibility: 'off'}]},
                {featureType: 'transit', stylers: [{visibility: 'off'}]},
                {featureType: 'administrative', stylers: [{visibility: 'off'}]},
                {featureType: 'administrative.locality', stylers: [{visibility: 'off'}]}
            ],
            type: 'roadmap'
        }),
        "Google POI": new L.gridLayer.googleMutant({
            styles: [
                {featureType: 'water', stylers: [{visibility: 'off'}]},
                {featureType: 'landscape', stylers: [{visibility: 'off'}]},
                {elementType: 'labels', stylers: [{visibility: 'off'}]},
                {featureType: 'road', stylers: [{visibility: 'off'}]},
                {featureType: 'poi', stylers: [{visibility: 'on'}]},
                {featureType: 'transit', stylers: [{visibility: 'off'}]},
                {featureType: 'administrative', stylers: [{visibility: 'off'}]},
                {featureType: 'administrative.locality', stylers: [{visibility: 'off'}]}
            ],
            type: 'roadmap'
        }),
        "Google Transist": new L.gridLayer.googleMutant({
            styles: [
                {featureType: 'water', stylers: [{visibility: 'off'}]},
                {featureType: 'landscape', stylers: [{visibility: 'off'}]},
                {elementType: 'labels', stylers: [{visibility: 'off'}]},
                {featureType: 'road', stylers: [{visibility: 'off'}]},
                {featureType: 'poi', stylers: [{visibility: 'off'}]},
                {featureType: 'transit', stylers: [{visibility: 'on'}]},
                {featureType: 'administrative', stylers: [{visibility: 'off'}]},
                {featureType: 'administrative.locality', stylers: [{visibility: 'off'}]}
            ],
            type: 'roadmap'
        }),
        "Google Water": new L.gridLayer.googleMutant({
            styles: [
                {featureType: 'water', stylers: [{visibility: 'on'}]},
                {featureType: 'landscape', stylers: [{visibility: 'off'}]},
                {elementType: 'labels', stylers: [{visibility: 'off'}]},
                {featureType: 'road', stylers: [{visibility: 'off'}]},
                {featureType: 'poi', stylers: [{visibility: 'off'}]},
                {featureType: 'transit', stylers: [{visibility: 'off'}]},
                {featureType: 'administrative', stylers: [{visibility: 'off'}]},
                {featureType: 'administrative.locality', stylers: [{visibility: 'off'}]}
            ],
            type: 'roadmap'
        })
    };
    return overlays;
};


/**
 * This method creates map container and adds base layers: Google, Yandex, Bing, OpenStreetMap
 * and google overlays to it
 */
AMapPage.prototype.generateLayers = function () {
    var yandexRoad = new L.Yandex('map');
    this.map = new L.Map(this.mapId, {center: this.initialPos, zoom: 13, layers: [yandexRoad]});
    var baseLayers = {
        "Google Sattelite": new L.gridLayer.googleMutant({type: 'satellite'}),
        "Google Road map": new L.gridLayer.googleMutant({type: 'roadmap'}),
        'Yandex Road map': yandexRoad,
        'Yandex Satellite': new L.Yandex('satellite'),
        'Yandex Hybrid': new L.Yandex('hybrid'),
        'Yandex Public map': new L.Yandex('publicMap'),
        'Yandex Public map hybrid': new L.Yandex('publicMapHybrid'),
        'Bing Aerial': new L.BingLayer('AkErn6IFlYKIm4Mp34p-ayPl_zTVk6LoUyp4J9HftaB_KJdDkBV6MmOV4eWKciNF',
            {type: 'Aerial'}),
        'Bing Aerial with labels': new L.BingLayer('AkErn6IFlYKIm4Mp34p-ayPl_zTVk6LoUyp4J9HftaB_KJdDkBV6MmOV4eWKciNF',
            {type: 'AerialWithLabels'}),
        'Bing Road': new L.BingLayer('AkErn6IFlYKIm4Mp34p-ayPl_zTVk6LoUyp4J9HftaB_KJdDkBV6MmOV4eWKciNF',
            {type: 'Road'}),
        'OpenStreetMap': new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png')
    };
    //Adding of base layers
    var baseLayersLen = baseLayers.length;
    for (var i = 0; i < baseLayersLen; i++)
        this.map.addLayer(baseLayers[i]);
    //Adding of overlays
    var overlays = this.getGoogleOverlays();
    //Adding of layers to map
    this.ctrl = L.control.layers(baseLayers, overlays).addTo(this.map);
};


/**
 * This method calls on creating each feature (add popups and binding parameters)
 */
AMapPage.prototype.onEachFeature = function (feature, layer) {
    layer.on('click', function (e) {

        MapPage.currentEditedLayerId = e.target._leaflet_id;

        if ('noise' in feature.properties) {
            document.getElementById("noise_block").style.display = "block";
            document.getElementById("noise").value = feature.properties.noise;
        }
        else {
            document.getElementById("noise_block").style.display = "none";
        }
        if ('slow_koef' in feature.properties) {
            document.getElementById("slow_koef_block").style.display = "block";
            document.getElementById("slow_koef").value = feature.properties.slow_koef;
        }
        else {
            document.getElementById("slow_koef_block").style.display = "none";
        }
        document.getElementById("descr").value = feature.properties.descr;

        document.getElementById("properties_objects").style.display = "block";
    });
    layer.bindPopup(feature.properties.descr);
};


AMapPage.prototype.updateParamsInLayer = function () {
    this.currentEditedOverlay.getLayer(this.currentEditedLayerId).bindPopup(document.getElementById("descr").value);
    this.currentEditedOverlay.getLayer(this.currentEditedLayerId).feature.properties.descr = document.getElementById("descr").value;
    if (document.getElementById("noise_block").style.display === "block"){
        this.currentEditedOverlay.getLayer(this.currentEditedLayerId).feature.properties.noise = document.getElementById("noise").value;
    }
    if (document.getElementById("slow_koef_block").style.display === "block"){
        this.currentEditedOverlay.getLayer(this.currentEditedLayerId).feature.properties.slow_koef = document.getElementById("slow_koef").value;
    }
    document.getElementById("properties_objects").style.display = "none";
};


/**
 *This method adds drawing overlays
 */
AMapPage.prototype.addDrawingOverlays = function () {
    switch (MapPage.permission) {
        case 'unauthorized':
            this.ctrl.addOverlay(this.tracks, 'Трэки групп');
            break;
        case 'sar_member':
            this.ctrl.addOverlay(this.planRoutes, 'Планирование маршрутов');
            this.ctrl.addOverlay(this.constObjects, 'Постоянные объекты');
            this.ctrl.addOverlay(this.tracks, 'Трэки групп');
            break;
        case 'sar_manager':
            this.ctrl.addOverlay(this.planRoutes, 'Планирование маршрутов');
            this.ctrl.addOverlay(this.constObjects, 'Постоянные объекты');
            this.ctrl.addOverlay(this.planVersions, 'Планирование версий');
            this.ctrl.addOverlay(this.tracks, 'Трэки групп');
            break;
    }
    // this.ctrl.addOverlay(this.planRoutes, 'Планирование маршрутов');
    // this.ctrl.addOverlay(this.constObjects, 'Постоянные объекты');
    // this.ctrl.addOverlay(this.planVersions, 'Планирование версий');
    // this.ctrl.addOverlay(this.tracks, 'Трэки групп');
};


/**
 *This method initializes features properties
 */
AMapPage.prototype.listOfFeaturesInit = function () {
    this.versions = [];
    this.routes = [];
    this.trackList = [];
    this.planRoutes = new L.GeoJSON(null, {
        onEachFeature: this.onEachFeature
    });
    this.constObjects = new L.GeoJSON(null, {
        onEachFeature: this.onEachFeature
    });
    this.planVersions = new L.GeoJSON(null, {
        onEachFeature: this.onEachFeature
    });
    this.tracks = new L.GeoJSON(null, {
        onEachFeature: this.onEachFeature
    });

    //in future permanent objects will be unchangeable
    this.constObjects.on("popupopen", function (e) {
        MapPage.currentEditedOverlay = MapPage.constObjects;
    });

    this.planRoutes.on("popupopen", function (e) {
        MapPage.currentEditedOverlay = MapPage.planRoutes;
    });

    this.planVersions.on("popupopen", function (e) {
        MapPage.currentEditedOverlay = MapPage.planVersions;
    });

    this.tracks.on("popupopen", function (e) {
        MapPage.currentEditedOverlay = MapPage.tracks;
    });

    this.currentOverlayForDrawingTools = this.planRoutes;
    this.delDivId = 'nonenonenone';

    this.actionsForDataFromServer = {
        setGeoDataToConstLayer: function (response) {
            for (var i = 0; i < response.length; i++){
                MapPage.constObjects.addData(response[i].gdata);
            }
        },
        setVersion: function (response) {
            for (var i = 0; i < response.length; i++){
                MapPage.versions.push(new L.GeoJSON(null, {
                    onEachFeature: MapPage.onEachFeature
                }));
                MapPage.planVersions.addData(response[i].gdata);
            }
            MapPage.version_descr = response[0].descr;
        },
        setRoute: function (response) {
            for (var i = 0; i < response.length; i++){
                MapPage.routes.push(new L.GeoJSON(null, {
                    onEachFeature: MapPage.onEachFeature
                }));
                MapPage.planRoutes.addData(response[i].gdata);
            }
            MapPage.route_descr = response[0].descr;
        },
        setTracksWithDescription: function (response) {
            for (var i = 0; i < response.length; i++){
                MapPage.tracks.addData(response[i].gdata);
                MapPage.trackList.push(response[i]);
            }
        }
    };
};


/**
 * This method initializes drawing control
 * @param currentOverlayForDrawingTools
 */
AMapPage.prototype.initializeDrawingControl = function (currentOverlayForDrawingTools) {

    this.draw_ctrl = new L.Control.Draw({
        edit: {
            featureGroup: currentOverlayForDrawingTools,

            poly: {
                allowIntersection: true
            }
        },
        draw: {
            featureGroup: currentOverlayForDrawingTools,
            polygon: {
                allowIntersection: false,
                showArea: true,
                shapeOptions: {
                    color: '#da1512'
                }
            },
            circle: false,
            rectangle: true,
            polyline: true
        }
    });
};


/**
 * This method adds overlay and drawing control fot it
 * @param overlay
 * @param control
 */
AMapPage.prototype.addDrawingControl = function (overlay, control) {
    this.map.on(L.Draw.Event.CREATED, function (e) {
        var geojson = e.layer.toGeoJSON();
        if (overlay === MapPage.planVersions && (e.layerType === "polyline" || e.layerType === "polygon" || e.layerType === "rectangle")){
            geojson.properties = {
                'descr': '',
                'noise': 0,
                'slow_koef': 0
            };
        } else {
            geojson.properties ={
                'descr': ''
            };
        }
        overlay.addData(geojson);
        $("#hideMapBtn").show();
    });
    this.map.addControl(control);
};


/**
 * This method deletes overlay and drawing control fot it
 * This method deletes overlay and drawing control fot it
 * @param overlay
 * @param control
 */
AMapPage.prototype.delDrawingControl = function (overlay, control) {
    this.map.on(L.Draw.Event.CREATED, function (e) {
        overlay.removeLayer(e.layer);
    });
    this.map.removeControl(control);
    // this.draw_ctrl.removeToolbar();
    console.log(this.map);
};


/**
 *This method turns tools of Planning on radio button click
 */
AMapPage.prototype.turnEditingOfPlanningVersionsOverlay = function () {
    $('#' + this.delDivId).parent().empty();
    // var delOverlay = this.currentOverlayForDrawingTools;
    // var delControl = this.draw_ctrl;
    // this.delDrawingControl(delOverlay, delControl);
    // this.currentOverlayForDrawingTools = this.planVersions;
    // var currentOverlay = this.currentOverlayForDrawingTools;
    // this.initializeDrawingControl(currentOverlay);
    // var draw_ctrl = this.draw_ctrl;
    //
    // this.addDrawingControl(currentOverlay, draw_ctrl);
    $('#overlay_tools_area').load('map-page-parts/versions_planning_tools.html');
    this.delDivId = 'versions_planning_tools';
};


AMapPage.prototype.editVersionsOnMap = function () {
    if (document.getElementById('version_button').innerHTML === "Редактировать объекты на карте") {
        var delOverlay = this.currentOverlayForDrawingTools;
        var delControl = this.draw_ctrl;
        this.delDrawingControl(delOverlay, delControl);
        this.currentOverlayForDrawingTools = this.planVersions;
        var currentOverlay = this.currentOverlayForDrawingTools;
        this.initializeDrawingControl(currentOverlay);
        var draw_ctrl = this.draw_ctrl;
        this.addDrawingControl(currentOverlay, draw_ctrl);
        document.getElementById('version_button').innerHTML = "Остановить редактирование карты"
    }
    else if (document.getElementById('version_button').innerHTML === "Остановить редактирование карты") {
        var delOverlay = this.currentOverlayForDrawingTools;
        var delControl = this.draw_ctrl;
        this.delDrawingControl(delOverlay, delControl);
        document.getElementById('version_button').innerHTML = "Редактировать объекты на карте"
    }
};


/**
 *This method turns tools of View on radio button click
 */
AMapPage.prototype.turnEditingOfViewOverlay = function () {
    // $('#' + this.delDivId).parent().empty();
    $('#overlay_tools_area').empty();
    var delOverlay = this.currentOverlayForDrawingTools;
    var delControl = this.draw_ctrl;
    this.delDrawingControl(delOverlay, delControl);

};


/**
 *This method turns tools of Tracks Planning on radio button click
 */
AMapPage.prototype.turnEditingOfPlanningTracksOverlay = function (divId, htmlPath, delDivId) {
    $('#' + this.delDivId).parent().empty();
    // this.delDivId = delDivId;
    // var delOverlay = this.currentOverlayForDrawingTools;
    // var delControl = this.draw_ctrl;
    // this.delDrawingControl(delOverlay, delControl);
    // this.currentOverlayForDrawingTools = this.planRoutes;
    // var currentOverlay = this.currentOverlayForDrawingTools;
    // this.initializeDrawingControl(currentOverlay);
    // var draw_ctrl = this.draw_ctrl;
    //
    // this.addDrawingControl(currentOverlay, draw_ctrl);

    $('#' + divId).load(htmlPath);
};


AMapPage.prototype.editRoutesOnMap = function () {
    if (document.getElementById('route_button').innerHTML === "Редактировать объекты на карте") {
        var delOverlay = this.currentOverlayForDrawingTools;
        var delControl = this.draw_ctrl;
        this.delDrawingControl(delOverlay, delControl);
        this.currentOverlayForDrawingTools = this.planRoutes;
        var currentOverlay = this.currentOverlayForDrawingTools;
        this.initializeDrawingControl(currentOverlay);
        var draw_ctrl = this.draw_ctrl;
        this.addDrawingControl(currentOverlay, draw_ctrl);
        document.getElementById('route_button').innerHTML = "Остановить редактирование карты"
    }
    else if (document.getElementById('route_button').innerHTML === "Остановить редактирование карты") {
        var delOverlay = this.currentOverlayForDrawingTools;
        var delControl = this.draw_ctrl;
        this.delDrawingControl(delOverlay, delControl);
        document.getElementById('route_button').innerHTML = "Редактировать объекты на карте"
    }
};


/**
 *This method turns tools of Tracks on radio button click
 */
AMapPage.prototype.turnEditingOfTracksOverlay = function () {
    $('#overlay_tools_area').load('map-page-parts/track_tools.html');
    $('#' + this.delDivId).parent().empty();
    var delOverlay = this.currentOverlayForDrawingTools;
    var delControl = this.draw_ctrl;
    this.delDrawingControl(delOverlay, delControl);
    this.delDivId = 'import_tracks_tools';
    // this.fillListOfTracks();
};


AMapPage.prototype.saveDescriptionVersion = function () {
    if (document.getElementById('version_name_button').innerHTML === "Редактировать примечание к версии") {
        document.getElementById('version_name').disabled = false;
        document.getElementById('version_name_button').innerHTML = "Сохранить примечание к версии"
    }
    else if (document.getElementById('version_name_button').innerHTML === "Сохранить примечание к версии") {
        document.getElementById('version_name').disabled = true;
        this.version_descr = document.getElementById('version_name').value;
        $("#hideMapBtn").show();
        document.getElementById('version_name_button').innerHTML = "Редактировать примечание к версии"
    }
};

AMapPage.prototype.saveDescriptionRoute = function () {
    if (document.getElementById('group_name_button').innerHTML === "Редактировать примечание к маршрутам") {
        document.getElementById('group_name').disabled = false;
        document.getElementById('group_name_button').innerHTML = "Сохранить примечание к маршрутам"
    }
    else if (document.getElementById('group_name_button').innerHTML === "Сохранить примечание к маршрутам") {
        document.getElementById('group_name').disabled = true;
        this.route_descr = document.getElementById('group_name').value;
        $("#hideMapBtn").show();
        document.getElementById('group_name_button').innerHTML = "Редактировать примечание к маршрутам"
    }
};


AMapPage.prototype.importTrackFromFile = function () {
    var file = document.getElementById("file").files[0];
    var reader = new FileReader();
    reader.onload = (function(file) {
        return function(e) {
            var parser = new DOMParser();
            var xmlDoc = parser.parseFromString(e.target.result, "text/xml");
            var geoJson = toGeoJSON.gpx(xmlDoc);
            if (document.getElementById('track_descr').value === '') {
                alert("Необходимо ввести описание трека");
            } else {
                MapPage.setTracksToServer(geoJson);
            }
        }
    })(file);
    reader.readAsText(file);
};


AMapPage.prototype.setTracksToServer = function(geojson){
    for (var i = 0; i < geojson.features.length; i++){
        geojson.features[i].properties.descr = document.getElementById('track_descr').value;
    }
    var track = {'gdata': geojson, 'id': parseInt(this.psrId), 'descr': document.getElementById('track_descr').value};
    MapPage.tracks.addData(geojson);
    MapPage.trackList.push(track);
    $.ajax({
        type: 'POST',
        url: "/app/track",
        data: JSON.stringify(track),
        success: function(data) { alert('data: ' + data); },
        contentType: "application/json",
        dataType: 'json'
    });
    $('#overlay_tools_area').load('map-page-parts/track_tools.html');
};


AMapPage.prototype.fillListOfTracks = function () {
    document.getElementById("listTracks").innerHTML = '';
    for (var i = 0; i < this.trackList.length; i++){
        var li = document.createElement("li");
        var inputValue = this.trackList[i]['descr'];
        var t = document.createTextNode(inputValue);
        li.setAttribute("id", "track" + i);
        li.appendChild(t);
        var span = document.createElement("SPAN");
        var txt = document.createTextNode("\u00D7");
        span.className = "del";
        span.appendChild(txt);
        span.onclick = function() {
            var id = parseInt(this.parentElement.getAttribute('id').slice(5));
            MapPage.trackList.splice(id, 1);
            MapPage.tracks.eachLayer(function (layer) {
                MapPage.tracks.removeLayer(layer);
            });
            for (var i = 0; i < MapPage.trackList.length; i++){
                MapPage.tracks.addData(MapPage.trackList[i].gdata);
            }
            MapPage.fillListOfTracks();
            $("#hideMapBtn").show();
        };
        if (MapPage.permission === 'sar_manager'){
            li.appendChild(span);
        }
        document.getElementById("listTracks").appendChild(li);
    }
};


/**
 * This method adds ruler to map
 */
AMapPage.prototype.addRuler = function () {
    var rulerScale = L.control.scale({
        maxWidth: 240,
        metric: true,
        imperial: false,
        position: 'bottomleft'
    }).addTo(this.map);
    var ruler = L.control.polylineMeasure({position: 'bottomleft', imperial: false}).addTo(this.map);
    this.map.addControl(rulerScale);
    this.map.addControl(ruler);
};


/**
 *
 * @param psr_num: id of psr
 * @param layer: layer_type {const, version, route, track}
 * @param responseHandler method for handling layers on leaflet
 * @return {Array} :geo data from response
 */
AMapPage.prototype.getGeoDataFromServer = function (psr_num, layer, responseHandler) {
    $.getJSON('/app/geodata?id=' + psr_num + '&layer=' + layer)
        .success(this.actionsForDataFromServer[responseHandler])
        .error(function (a) {
            if (a.status !=403){
                alert("Ошибка получения геоданных для ПСР №" + MapPage.psrId);
            }
        });
};


AMapPage.prototype.sendAllGeoDataToServer = function () {
    this.setGeoDataToServer('version', this.planVersions.toGeoJSON());
    this.setGeoDataToServer('route', this.planRoutes.toGeoJSON());
    this.setGeoDataToServer('track', this.trackList);
    $("#hideMapBtn").hide();
};


/**
 * This method sends JSON string with GeoJSON in it to server
 * @param layer_type
 * @param geojson
 */
AMapPage.prototype.setGeoDataToServer = function (layer_type, geojson) {
    if (layer_type === 'track') {
        $.ajax({
            type: 'POST',
            url: "/app/geodata?id=" + this.getUrlVars("id") + '&layer=' + layer_type,
            data: JSON.stringify(geojson),
            success: function(data) { alert('data: ' + data); },
            contentType: 'application/json',
            dataType: 'json'
        });
    }
    else if (layer_type === 'route'){
        $.ajax({
            type: 'POST',
            url: "/app/geodata?id=" + this.getUrlVars("id") + '&layer=' + layer_type,
            data: JSON.stringify({'gdata': geojson, 'descr': this.route_descr}),
            success: function(data) { alert('data: ' + data); },
            contentType: 'application/json',
            dataType: 'json'
        });
    }
    else if (layer_type === 'version'){
        $.ajax({
            type: 'POST',
            url: "/app/geodata?id=" + this.getUrlVars("id") + '&layer=' + layer_type,
            data: JSON.stringify({'gdata': geojson, 'descr': this.version_descr}),
            success: function(data) { alert('data: ' + data); },
            contentType: 'application/json',
            dataType: 'json'
        });
    }
};

/**
 *This method gets vars from URL
 *
 * @inner string name
 * @return string result
 */
AMapPage.prototype.getUrlVars = function (name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results == null)
        return "";
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
};


AMapPage.prototype.checkPermissions = function () {
    $.when($.get('/app/check')
        .success(function (data) {
            MapPage.permission = data;
        })).done(function () {
        switch (MapPage.permission) {
            case 'unauthorized':
                document.getElementById("control").style.display = "none";
                document.getElementById("control").style.width = "0%";
                document.getElementById("map_layer").style.width = "100%";
                break;
            case 'sar_member':
                document.getElementById("only_mngr").style.display = "none";
                $("#hideMapBtn").hide();
                document.getElementById("control").style.width = "15%";
                document.getElementById("map_layer").style.width = "85%";
                break;
            case 'sar_manager':
                // $("#hideMapBtn").show();
                $("#hideMapBtn").hide();
                document.getElementById("only_mngr").style.display = "block";
                document.getElementById("control").style.width = "15%";
                document.getElementById("map_layer").style.width = "85%";
                break;
        }
        MapPage.setupPage('map_layer');
    });
};


/**
 * This method setups map
 * @inner string mapId: html id of map container
 */
AMapPage.prototype.setupPage = function (mapId) {
    this.psrId = this.getUrlVars("id");
    var pos = [60.02663, 30.542937];
    $.getJSON('/app/info?id=' + this.psrId)
        .success(function (data) {
            pos = [parseFloat(data.lat), parseFloat(data.long)];

        })
        .error(function (data) {
            if (data.status !=403){
                alert("Ошибка получения геоданных для ПСР №" + MapPage.psrId);
            }
        });
    this.initialPos = pos;
    this.mapId = mapId;
    this.generateLayers();
    this.listOfFeaturesInit();
    this.initializeDrawingControl();
    this.addDrawingOverlays();
    this.addRuler();

    //For editing parameters of geo objects
    this.currentEditedLayerId = undefined;
    this.currentEditedOverlay = undefined;
    this.map.on('click', function () {
        document.getElementById("properties_objects").style.display = "none";
    });
    ///////////////////////////////////////
    switch (MapPage.permission) {
        case 'unauthorized':
            this.getGeoDataFromServer(this.psrId, 'track', 'setTracksWithDescription');
            break;
        case 'sar_member':
            this.getGeoDataFromServer(this.psrId, 'const', 'setGeoDataToConstLayer');  // You should add your handlers at actionsForDataFromServer in listOfFeaturesInit
            this.getGeoDataFromServer(this.psrId, 'route', 'setRoute');
            this.getGeoDataFromServer(this.psrId, 'track', 'setTracksWithDescription');
            break;
        case 'sar_manager':
            this.getGeoDataFromServer(this.psrId, 'const', 'setGeoDataToConstLayer');  // You should add your handlers at actionsForDataFromServer in listOfFeaturesInit
            this.getGeoDataFromServer(this.psrId, 'version', 'setVersion');
            this.getGeoDataFromServer(this.psrId, 'route', 'setRoute');
            this.getGeoDataFromServer(this.psrId, 'track', 'setTracksWithDescription');
            break;
    }
    // this.getGeoDataFromServer(this.psrId, 'const', 'setGeoDataToConstLayer');  // You should add your handlers at actionsForDataFromServer in listOfFeaturesInit
    // this.getGeoDataFromServer(this.psrId, 'version', 'setVersion');
    // this.getGeoDataFromServer(this.psrId, 'route', 'setRoute');
    // this.getGeoDataFromServer(this.psrId, 'track', 'setTracksWithDescription');
};