#!/usr/bin/python3
# -*- coding: utf-8 -*-
import psycopg2.extras
import time
from datetime import datetime
import json


class Db:
    def __init__(self):
        hostname = 'localhost'
        username = 'rescue_user'
        password = 'rescue_user'
        database = 'rescue_db'
        current_time = time.strftime("%a, %d-%b-%Y %T GMT: ", time.gmtime(time.time()))
        try:
            self.connection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
            print(current_time + 'Connect to DB is successful')
        except Exception:
            print(current_time + 'Something is wrong  with connect to DB')

    def __del__(self):
        self.connection.close()
        current_time = time.strftime("%a, %d-%b-%Y %T GMT: ", time.gmtime(time.time()))
        print(current_time + 'Connection to DB is closed')

    def get_id_org_by_name_org(self, name_org):
        """
        Checking name of organisation and return id of organisation
        :param name_org: Name of organisation
        :return: Id of organisation
        """
        with self.connection.cursor() as cursor:
            try:
                cursor.callproc('get_id_org_by_name_org', [name_org, ])
                org_id = cursor.fetchone()
                return org_id[0]
            except psycopg2.InternalError:
                self.connection.rollback()

    def get_list(self, offset=0, count=10):
        """
        Getting list of SAR OPs from database
        :param offset: Start index for returned data
        :param count: Number of returned fields
        :return: RealDict object with needed fields
        """
        with self.connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cursor:
            try:
                cursor.callproc('get_sar_ops', [offset, count])
                operations = cursor.fetchall()
                for operation in operations:
                    operation['start_date'] = operation['start_date'].strftime('%d.%m.%Y')
                return operations
            except psycopg2.InternalError:
                self.connection.rollback()

    def get_list_active(self, offset=0, count=10):
        """
        Getting list of active SAR OPs from database
        :param offset: Start index for returned data
        :param count: Number of returned fields
        :return: RealDict object with needed fields
        """
        with self.connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cursor:
            try:
                cursor.callproc('get_sar_ops_active', [offset, count])
                operations = cursor.fetchall()
                for operation in operations:
                    operation['start_date'] = operation['start_date'].strftime('%d.%m.%Y')
                return operations
            except psycopg2.InternalError:
                self.connection.rollback()

    def get_info(self, num_sar=1):
        """
        Getting SAR OP information from database by its num
        :param num_sar: Number of SAR OP
        :return: RealDict object with needed field or None object if num_sar not found in database
        """
        with self.connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cursor:
            try:
                cursor.callproc('get_info_of_sar_op_by_num', [num_sar, ])
                operation = cursor.fetchone()
                if operation:
                    operation['start_date'] = operation['start_date'].strftime('%d.%m.%Y')
                    return operation
                else:
                    raise AttributeError
            except psycopg2.InternalError:
                self.connection.rollback()

    def add_short_info(self, short_descr, org_id, lat, long):
        """
        Creating new sar operation with short info
        :param short_descr: Short description of sar operation
        :param org_id: Id of organisation
        :param lat: Latitude of coordinates of area for sar operation
        :param long: Longitude of coordinates of area for sar operation
        """
        with self.connection.cursor() as cursor:
            try:
                cursor.callproc('add_sar_op', [short_descr, org_id, lat, long])
                self.connection.commit()
            except psycopg2.InternalError:
                self.connection.rollback()

    def update_info(self, sar_num, descr, start_date, status, general_info, private_info, secondary_info, lat, long):
        """
        Updating of information of sar operation
        :param sar_num: Identified number for sar operation
        :param descr: Short description of sar operation
        :param start_date: Start date for sar operation
        :param status: Status of sar operation
        :param general_info: Primary info of sar operation
        :param private_info: Closed info of sar operation
        :param secondary_info: Secondary info of sar operation
        :param lat: Latitude of coordinates of area for sar operation
        :param long: Longitude of coordinates of area for sar operation
        """
        with self.connection.cursor() as cursor:
            try:
                start_date = datetime.strptime(start_date, '%d.%m.%Y').date()
                cursor.callproc('update_info_of_sar_op_by_num',
                                [sar_num, descr, start_date, status, general_info, private_info, secondary_info, lat,
                                 long])
                self.connection.commit()
            except psycopg2.InternalError:
                self.connection.rollback()

    def get_reminders(self, num_sar=1):
        """
        Get list of reminders for SAR operation
        :param num_sar: ID of SAR operation
        :return: RealDict object which contains list of reminders
        """
        with self.connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cursor:
            try:
                cursor.callproc('get_reminders_by_sar_num', [num_sar, ])
                reminders = cursor.fetchall()
                for reminder in reminders:
                    reminder['create_date'] = reminder['create_date'].strftime('%Y-%m-%d %H:%M:%S')
                    if reminder['alarm']:
                        reminder['alarm'] = reminder['alarm'].strftime('%Y-%m-%d %H:%M:%S')
                return reminders
            except psycopg2.InternalError:
                self.connection.rollback()

    def add_reminder_no_alarm(self, num_sar, description):
        """
        Add reminder w/o alarm
        :param num_sar: ID of SAR operation
        :param description: Text description of reminder
        """
        with self.connection.cursor() as cursor:
            try:
                cursor.callproc('add_reminder_no_alarm_by_sar_num', [num_sar, description])
                self.connection.commit()
            except psycopg2.InternalError:
                self.connection.rollback()

    def add_reminder_alarm(self, num_sar, alarm_in_hours, description):
        """
        Add alarm of reminder
        :param num_sar: ID of SAR operation
        :param alarm_in_hours: Time to alarm
        :param description: Text description of reminder
        """
        with self.connection.cursor() as cursor:
            try:
                cursor.callproc('add_reminder_alarm_by_sar_num', [num_sar, alarm_in_hours, description])
                self.connection.commit()
            except psycopg2.InternalError:
                self.connection.rollback()

    def add_reminder(self, num_sar, create_date, alarm, description, active):
        """
        Add full-described and any type reminder
        :param num_sar: ID of SAR operation
        :param create_date: datetime of creating reminder
        :param alarm: datetime of alarming this reminder
        :param description: Text description of reminder
        :param active: Status of reminder
        """
        with self.connection.cursor() as cursor:
            try:
                active = active in ['true', 'True', 't', 'y']
                cursor.callproc('add_reminder_by_sar_num', [num_sar, create_date, alarm, description, active])
                self.connection.commit()
            except psycopg2.InternalError:
                self.connection.rollback()

    def update_reminder(self, rem_id, create_date, alarm, description, active):
        """
        Update reminder by its ID
        :param rem_id: ID of reminder
        :param create_date: datetime of creating reminder
        :param alarm: datetime of alarming this reminder
        :param description: Text description of reminder
        :param active: Status of reminder
        """
        with self.connection.cursor() as cursor:
            try:
                active = active in ['true', 'True', 't', 'y']
                cursor.callproc('update_reminder_by_id', [rem_id, create_date, alarm, description, active])
                self.connection.commit()
            except psycopg2.InternalError:
                self.connection.rollback()

    def reset_reminder(self, rem_id):
        """
        Reset reminder status by its ID
        :param rem_id: ID of reminder
        """
        with self.connection.cursor() as cursor:
            try:
                cursor.callproc('reset_reminder_by_id', [rem_id, ])
                self.connection.commit()
            except psycopg2.InternalError:
                self.connection.rollback()

    def check_user(self, login):
        """
        Check existing of login
        :param login: checked login
        :return: True or False == Exist or not Exist
        """
        with self.connection.cursor() as cursor:
            try:
                cursor.callproc('check_user_by_login', [login, ])
                exists = cursor.fetchone()
                return exists[0]
            except psycopg2.InternalError:
                self.connection.rollback()

    def logon_user(self, login, password):
        """
        Check existing of login-password
        :param login: checked login
        :param password: checked hash of password
        :return: Some data about logged user (status, name of user)
        """
        with self.connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cursor:
            try:
                cursor.callproc('logon_person', [login, password])
                op = cursor.fetchone()
                return op
            except psycopg2.InternalError:
                self.connection.rollback()

    def get_hash_pwd(self, login):
        """
        Getter hash of password for some user
        :param login: checked login
        :return: string hash with len(128)
        """
        with self.connection.cursor() as cursor:
            try:
                cursor.callproc('get_hash_by_login', [login, ])
                hash_pwd = cursor.fetchone()
                return hash_pwd[0]
            except psycopg2.InternalError:
                self.connection.rollback()

    def get_geo_data(self, num_sar, layer_type):
        """
        Getting geo data from database by identifier of sar operation and layer type
        :param num_sar: Identifier of sar operation
        :param layer_type: Type of layer
        :return: List of dictionaries with geo data
        """
        with self.connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cursor:
            try:
                if layer_type == 'const':
                    cursor.callproc('get_const_geo_data', [])
                else:
                    cursor.callproc('get_geo_data_by_sar_num_and_layer_type', [num_sar, layer_type, ])
                geo_data = cursor.fetchall()
                return geo_data
            except psycopg2.InternalError:
                self.connection.rollback()

    def set_geo_data_to_db(self, num_sar, layer_type, geo_data_json):
        """
        Setting geo data to database by identifier of sar operation and layer type
        :param num_sar: Identifier of sar operation
        :param layer_type: Type of layer
        :param geo_data_json: List of dictionaries with geo data
        """
        with self.connection.cursor() as cursor:
            try:
                cursor.callproc('delete_geo_data_by_sar_num_and_layer_type', [num_sar, layer_type])
                if layer_type == 'const':
                    pass
                elif type(geo_data_json) is dict:
                    cursor.callproc('set_geo_data_by_sar_num_and_layer_type',
                                    [num_sar, layer_type, json.dumps(geo_data_json['gdata']), geo_data_json['descr']])
                elif type(geo_data_json) is list:
                    for layer in geo_data_json:
                        cursor.callproc('set_geo_data_by_sar_num_and_layer_type',
                                        [num_sar, layer_type, json.dumps(layer['gdata']), layer['descr']])
                self.connection.commit()
            except psycopg2.InternalError:
                self.connection.rollback()

    def set_track_to_db(self, track_json):
        """
        Setting tracks to database by track info
        :param track_json: JSON with needed fields
        """
        with self.connection.cursor() as cursor:
            try:
                cursor.callproc('set_geo_data_by_sar_num_and_layer_type',
                                [track_json['id'], 'track', json.dumps(track_json['gdata']), track_json['descr']])
                self.connection.commit()
            except psycopg2.InternalError:
                self.connection.rollback()


if __name__ == '__main__':
    db = Db()
    db.update_info(int("4"), "updated descr for 4 sar op", "01.05.2017", "waiting", "primary info", "private info",
                   "secondary info", float("30.0"), float("60.0"))
