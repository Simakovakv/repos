package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.Random;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TestAuth {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  String[] user = {"user1", "user2", "user3"};
  Random rand = new Random((long)user.length);
  
  @Before
  public void setUp() throws Exception {
	System.setProperty("webdriver.gecko.driver","/home/ksenia/eclipse/geckodriver");
	driver = new FirefoxDriver();
    baseUrl = "http://localhost/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    
   }
  @Test
  public void test1Neg41() throws Exception {
	String ermsg = "Пожалуйста, заполните это поле";
	  
    driver.get(baseUrl + "/index.html");
    driver.findElement(By.id("user")).clear();
    driver.findElement(By.id("pwd")).clear();
    driver.findElement(By.name("submit")).click();
    List<WebElement>erromsgsize = driver.findElements(By.className("errorValidation"));
    for (WebElement errormsg : erromsgsize) {

    try {
    	Assert.assertTrue(errormsg.getText().equals(ermsg));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
  }
  }
  
//  @Test
//  public void test1Pos43() throws Exception {
//    driver.get(baseUrl + "/index.html");
//    driver.findElement(By.id("user")).clear();
//    driver.findElement(By.id("user")).sendKeys(user[rand.nextInt()]);
//    driver.findElement(By.id("pwd")).clear();
//    driver.findElement(By.id("pwd")).sendKeys("123");
//    driver.findElement(By.name("submit")).click();
//    driver.findElement(By.xpath("//button[@onclick='logout()']")).click();
//    Thread.sleep(5000);
//    try {
//    	assertTrue(isElementPresent(By.id("auth-form")));
//    } catch (Error e) {
//      verificationErrors.append(e.toString());
//    }
//  }

  @Test
  public void test1Pos44() throws Exception {
    driver.get(baseUrl + "/index.html");
    driver.findElement(By.id("user")).clear();
    driver.findElement(By.id("user")).sendKeys("user1");
    driver.findElement(By.id("pwd")).clear();
    driver.findElement(By.id("pwd")).sendKeys("123");
    driver.findElement(By.name("submit")).click();
    Thread.sleep(5000);
    try {
      assertTrue(driver.findElement(By.id("intro_user")).getText().contains("Just Member"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
  }

  @Test
  public void test1Pos45() throws Exception {
    driver.get(baseUrl + "/index.html");
    driver.findElement(By.id("user")).clear();
    driver.findElement(By.id("user")).sendKeys("user2");
    driver.findElement(By.id("pwd")).clear();
    driver.findElement(By.id("pwd")).sendKeys("123");
    driver.findElement(By.name("submit")).click();
    Thread.sleep(5000);
    try {
        assertTrue(driver.findElement(By.id("intro_user")).getText().contains("Sar1 Manager"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
  }
  @Test
  public void test1Pos39() throws Exception {
    driver.get(baseUrl + "/index.html");
    driver.findElement(By.id("user")).clear();
    driver.findElement(By.id("user")).sendKeys("user2");
    driver.findElement(By.id("pwd")).clear();
    driver.findElement(By.id("pwd")).sendKeys("123");
    driver.findElement(By.name("submit")).click();
    Thread.sleep(5000);
    try {
    	assertTrue(driver.findElement(By.id("openAddForm")).isDisplayed());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
  }
  
  @Test
  public void test1Pos46() throws Exception {
    driver.get(baseUrl + "/index.html");
    driver.findElement(By.id("user")).clear();
    driver.findElement(By.id("user")).sendKeys("user3");
    driver.findElement(By.id("pwd")).clear();
    driver.findElement(By.id("pwd")).sendKeys("123");
    driver.findElement(By.name("submit")).click();
    Thread.sleep(5000);
    try {
        assertTrue(driver.findElement(By.id("intro_user")).getText().contains("Sar2 Manager"));
      System.out.println("Login is success");
    } catch (Error e) {
      verificationErrors.append(e.toString());
      System.out.println("Login is fail");
    }
  }
  @Test
  public void test1Pos42() throws Exception {
    driver.get(baseUrl + "/index.html");
    driver.findElement(By.id("user")).clear();
    driver.findElement(By.id("user")).sendKeys("user3");
    driver.findElement(By.id("pwd")).clear();
    driver.findElement(By.id("pwd")).sendKeys("123");
    driver.findElement(By.name("submit")).click();
    Thread.sleep(5000);
    try {
    	assertTrue(driver.findElement(By.id("openAddForm")).isDisplayed());
      System.out.println("Login is success");
    } catch (Error e) {
      verificationErrors.append(e.toString());
      System.out.println("Login is fail");
    }
  }  
  @Test
  public void test1Pos47() throws Exception {
    driver.get(baseUrl + "/index.html");
    driver.findElement(By.id("user")).clear();
    driver.findElement(By.id("pwd")).clear();
    try {
    	assertFalse(driver.findElement(By.id("openAddForm")).isDisplayed());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
  }
  @Test
  public void test1Pos48() throws Exception {
     	driver.get(baseUrl + "/info.html?id=1");
	    driver.findElement(By.id("user")).clear();
	    driver.findElement(By.id("pwd")).clear();
	    try {
	    	assertFalse(driver.findElement(By.id("openEditForm")).isDisplayed());
	    } catch (Error e) {
	      verificationErrors.append(e.toString());
	    }
	 } 
  @Test
  public void test1Pos49() throws Exception {
     	driver.get(baseUrl + "/info.html?id=1");
	    driver.findElement(By.id("user")).clear();
	    driver.findElement(By.id("pwd")).clear();
	    try {
	    	assertFalse(driver.findElement(By.id("authentication-required")).isDisplayed());
	    } catch (Error e) {
	      verificationErrors.append(e.toString());
	    }
	 } 
  @Test
  public void test1Pos50() throws Exception {
	   driver.get(baseUrl + "/info.html?id=1");
	    driver.findElement(By.id("user")).clear();
	    driver.findElement(By.id("user")).sendKeys("user1");
	    driver.findElement(By.id("pwd")).clear();
	    driver.findElement(By.id("pwd")).sendKeys("123");
	    driver.findElement(By.name("submit")).click();
	    
	    try {
	    	assertTrue(driver.findElement(By.id("authentication-required")).isDisplayed()); 
	    } catch (Error e) {
	      verificationErrors.append(e.toString());
	    }
	  } 
  
  @Test
  public void test1Pos51() throws Exception {
	   driver.get(baseUrl + "/info.html?id=1");
	    driver.findElement(By.id("user")).clear();
	    driver.findElement(By.id("user")).sendKeys("user2");
	    driver.findElement(By.id("pwd")).clear();
	    driver.findElement(By.id("pwd")).sendKeys("123");
	    driver.findElement(By.name("submit")).click();
	    
	    try {
	    	assertTrue(driver.findElement(By.id("openEditForm")).isDisplayed()); 
	    } catch (Error e) {
	      verificationErrors.append(e.toString());
	    }
	  } 
	 
  @Test
  public void test1Pos52() throws Exception {
	   driver.get(baseUrl + "/info.html?id=1");
	    driver.findElement(By.id("user")).clear();
	    driver.findElement(By.id("user")).sendKeys("user2");
	    driver.findElement(By.id("pwd")).clear();
	    driver.findElement(By.id("pwd")).sendKeys("123");
	    driver.findElement(By.name("submit")).click();
	    
	    try {
	    	assertTrue(driver.findElement(By.id("authentication-required")).isDisplayed()); 
	    } catch (Error e) {
	      verificationErrors.append(e.toString());
	    }
	  } 
  @Test
  public void test1Pos53() throws Exception {
	   driver.get(baseUrl + "/info.html?id=1");
	    driver.findElement(By.id("user")).clear();
	    driver.findElement(By.id("user")).sendKeys("user3");
	    driver.findElement(By.id("pwd")).clear();
	    driver.findElement(By.id("pwd")).sendKeys("123");
	    driver.findElement(By.name("submit")).click();
	    
	    try {
	    	assertTrue(driver.findElement(By.id("openEditForm")).isDisplayed()); 
	    } catch (Error e) {
	      verificationErrors.append(e.toString());
	    }
	  } 
	 
  @Test
  public void test1Pos54() throws Exception {
	   driver.get(baseUrl + "/info.html?id=1");
	    driver.findElement(By.id("user")).clear();
	    driver.findElement(By.id("user")).sendKeys("user3");
	    driver.findElement(By.id("pwd")).clear();
	    driver.findElement(By.id("pwd")).sendKeys("123");
	    driver.findElement(By.name("submit")).click();
	    
	    try {
	    	assertTrue(driver.findElement(By.id("authentication-required")).isDisplayed()); 
	    } catch (Error e) {
	      verificationErrors.append(e.toString());
	    }
	  } 
  
  
  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}

	 