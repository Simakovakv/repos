/*Задача: покрыть отрезки точками
Выведите разложение натурального числа n > 1 на простые множители.
Простые множители должны быть упорядочены по возрастанию и разделены пробелами.

Input: 75
Output: 3 5 5
*/
#include <iostream>
#include <math.h>

using namespace std;
bool isPrime(int num) {
    if (num == 1) return false;
    for (int i = 2; i < num; ++i)
    {
            if(num % i == 0) return false;
    }
    return true;
}

int setPrime (int num)
{
    if (isPrime(num))
        return num;
    return setPrime(num += 1);
}

int main() {
    int n, result = 2;

    cin >> n;
    while (!(n==1) && n != 0)
    {
        if (n % result == 0)
        {
            n /= result;
            cout << result << " ";
        }
        else
            result = setPrime(result += 1);
    }
    return 0;
}
