/*Задача: покрыть отрезки точками

По данным n отрезкам необходимо найти множество точек минимального размера,
для которого каждый из отрезков содержит хотя бы одну из точек.
В первой строке дано число 1≤n≤100 отрезков.
Каждая из последующих n строк содержит по два числа 0≤l≤r≤109,
задающих начало и конец отрезка. Выведите оптимальное число m точек и сами m точек.
Если таких множеств точек несколько, выведите любое из них.
Sample Input:
4
4 7
1 3
2 5
5 6
Sample Output:
2
3 6
*/
#include <iostream>
#include <vector>
#include <string>
#include <math.h>
#include <cstdlib>
#include <vector>
#include <algorithm>

using namespace std;
int main()
{
    int num = 0, start = 0, end = 0;
    cin >> num;
    vector<pair<int, int>> segments;
    for (int i = 0; i < num; i++)
    {
        cin >> start >> end;
        segments.push_back({start, end});
    }
    sort(segments.begin(), segments.end(), [](pair<int, int> a, pair<int, int> b) { return a.second < b.second; });
    vector <int> points;
    for (const auto& seg : segments)
    {
        if (points.empty())
            points.push_back(seg.second);
        else if (points.back() >= seg.first && points.back() <= seg.second)
            continue;
        else
            points.push_back(seg.second);
    }
    cout << points.size() << endl;
    for(const auto& p : points)
        cout << p << " ";
    return 0;
}



































