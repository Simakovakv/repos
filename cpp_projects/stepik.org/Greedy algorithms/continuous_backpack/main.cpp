/*
непрерывный рюкзак

Первая строка содержит количество предметов 1≤n≤103 и вместимость рюкзака 0≤W≤2⋅106.
Каждая из следующих n строк задаёт стоимость 0≤ci≤2⋅106 и объём 0<wi≤2⋅106 предмета (n, W, ci, wi — целые числа).
Выведите максимальную стоимость частей предметов (от каждого предмета можно отделить любую часть, стоимость и
объём при этом пропорционально уменьшатся), помещающихся в данный рюкзак, с точностью не менее трёх знаков после запятой.
Sample Input:
3 50
60 20
100 50
120 30
Sample Output:
180.000
*/
#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>

using namespace std;

double test(vector <pair<double, double>> vec, double capacity, double result) {
    for (const auto& x : vec)
    {
        if(capacity >= x.second)
        {
            result += x.first;
            capacity -= x.second;
        }
        else if (capacity > 0)
        {
            result += capacity * (x.first / x.second);
            break;
        }
    }
    return result;
}

int main()
{
    double count, capacity, cost, size, result = 0;
    vector <pair<double, double>> c_v;
    vector <int> prices;
    cin >> count >> capacity;
    for (int i = 0; i < count; i ++)
    {
        cin >> cost >> size;
        c_v.push_back({cost, size});
    }
    sort(c_v.begin(), c_v.end(), [](pair<double, double> a, pair<double, double> b) { return a.first/a.second > b.first / b.second; });
    cout << fixed << setprecision(3) << test(c_v, capacity, result );
    return 0;
}
