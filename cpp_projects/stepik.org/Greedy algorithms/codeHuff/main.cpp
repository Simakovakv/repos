/*
кодирование Хаффмана. По данной непустой строке s длины не более 104, состоящей из строчных букв латинского алфавита,
постройте оптимальный беспрефиксный код. В первой строке выведите количество различных букв k, встречающихся в строке,
и размер получившейся закодированной строки. В следующих k строках запишите коды букв в формате "letter: code".
В последней строке выведите закодированную строку.

Sample Input 1:
a
Sample Output 1:
1 1
a: 0
0
Sample Input 2:
abacabad
Sample Output 2:
4 14
a: 0
b: 10
c: 110
d: 111
01001100100111
*/

#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

int main()
{
    string line, temp = "";
    long freq = 0;
    vector <long> vFreq;
    cin >> line;
    sort(line.begin(), line.end());
    temp = line;
    temp.erase(unique(temp.begin(), temp.end()), temp.end());
    cout << temp << " " << temp.size() << endl;
    for (const auto& ch : temp)
    {
        freq = count(line.begin(),line.end(), ch);
        vFreq.push_back(freq);
        cout << ch << " : " << freq << endl;
        line.erase(line.begin(), line.begin() + count(line.begin(),line.end(), ch));
        cout << line << endl;
    }

    cout << line;
    return 0;
}
