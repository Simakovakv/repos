/*
По данному числу 1≤n≤109 найдите максимальное число k, для которого n можно представить
как сумму k различных натуральных слагаемых. Выведите в первой строке число k, во второй — k слагаемых.
Sample Input 1:
4
Sample Output 1:
2
1 3
Sample Input 2:
6
Sample Output 2:
3
1 2 3
*/

#include <iostream>
#include <vector>

using namespace std;

int main()
{
    long num, temp = 0;
    vector <long> store;
    cin >> num;
    temp = num;
    for (long i = 1; i <= num; i++)
    {
        temp -= i;
        if (temp <= i)
        {
            store.push_back(i + temp);
            break;
        }
        else
            store.push_back(i);
    }
    cout << store.size() << endl;
    for (const auto& s : store)
    {
        cout << s << " ";
    }
}
