/* Simple CPP Program to print Fibonacci sequence from 1 to 40 using 3 variables (1, 1, 2, 3, )
 * time of 10th-40th num = 0,005s
*/
#include <iostream>
#include <string>
#include <math.h>
#include <cstdlib>

using namespace std;

static int calc_fib (int num, int pp, int p) {
    for (int count = 1; count < num; count++) {
        int current = pp + p;
        pp = p;
        p = current;
    }
    return p;
}

int main()
{
    int fib_num;
    int prevprev_fib = 0, prev_fib = 1;
    cout << "Enter any int number from 1 to 40" << endl;
    cin >> fib_num;
    if (fib_num <= 0)
        cout << prevprev_fib << endl;
    else if (fib_num == 1)
        cout << prev_fib << endl;
    else
        cout << calc_fib(fib_num, prevprev_fib, prev_fib) << endl;
    return 0;
}
