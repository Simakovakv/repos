#include <iostream>
#include <string>
#include <math.h>
#include <cstdlib>

using namespace std;

int main()
{
    int int_num;
    int prevprev_fib = 0, prev_fib=1;
    int count = 1;
    cout << "Enter any int number from 1 to 10^7" << endl;
    cin >> int_num;
    if (int_num == 1)
        cout << prevprev_fib << endl;
    else {
        while (count < int_num)
        {
            int temp = prevprev_fib % 10;
            prevprev_fib = prev_fib % 10;
            prev_fib += temp;
            count++;
        }
        cout << prev_fib %10 << endl;
    }

    return 0;
}
