// Даны целые числа 1 < n < 10^18 и 2 < m < 10^5, необходимо найти остаток от деления n-го числа Фибоначчи на m.

#include <iostream>
#include <vector>

using namespace std;
int main()
{
    size_t fib_num = 0;
    size_t div_num = 0;
    cin >> fib_num >> div_num;
    vector<size_t> vFib(2);
    vFib[0] = 0;
    vFib[1] = 1;
    size_t count;
    for (count = 2; count <= div_num*6; ++count) {
        vFib.push_back((vFib[count - 1] + vFib[count - 2]) % div_num);
        if ((vFib[count] == 1) && (vFib[count - 1] == 0))
        {
            vFib.erase(vFib.end() - 2, vFib.end());
            count = count-2;
            break;
        }
    }
    size_t index = fib_num % vFib.size();
    if (index != 0)
        cout << vFib[index] << endl;
    else
        cout << vFib[(index+vFib.size()) % vFib.size()] << endl;
    return 0;
}
