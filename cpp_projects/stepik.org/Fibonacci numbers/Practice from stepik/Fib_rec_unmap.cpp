/* Recursive calls for fibonacci sequence using unordered map
time up to 40th num = 0,005s
time of 100000th num = 0,084s
up to 262000th num core dumped
*/
#include <iostream>
#include <cassert>
#include <unordered_map>

class Fibonacci final {
	public:
		static int get(int n) {
			assert(n >=0);
			if (n == 0 || n == 1) {
				return n;
			}

			static std::unordered_map<int, int> cache;
			auto &result = cache[n];
			if (result == 0) {
				result = get(n - 2) + get(n - 1);
			}
			return result;
		}
};

int main(void) {
	int n;
	std::cin >> n;
	std::cout << Fibonacci::get(n) << std::endl;
	return 0;
}
