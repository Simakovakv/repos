/* Simple CPP Program to print Fibonacci sequence from 1 to 40 using 3 variables (1, 1, 2, 3, )
 * time of 10th-40th num = 0,005s
 * time of 100000th num = 0,007s (contingently)
 * time of 1000000th num = 0,017s (contingently)
*/
#include <cassert>
#include <iostream>

class Fibonacci final {
	public:
		static int get(int n) {
			assert(n >= 0);
			if (n<=1) {
				return n;
			}
			int previous = 0;
			int current = 1;
			for (int i = 2; i <= n; i++) {
				int new_current = previous + current;
				previous = current;
				current = new_current;
			}
			return current;
		}
};

int main(void) {
	int n;
	std::cin >> n;
	std::cout << Fibonacci::get(n) << std::endl;
	return 0;
}
