/* Recursive calls for fibonacci sequence
time of 10th num = 0,005s
time of 30th num = 0.038s
time of 40th num = 0,919s
*/
#include <iostream>
#include <cassert>

class Fibonacci final {
	public:
		static int get(int n) {
			assert(n >=0);
			if (n == 0 || n == 1) {
				return n;
			}
			return get(n - 2) + get(n - 1);
		}
};

int main(void) {
	int n;
	std::cin >> n;
	std::cout << Fibonacci::get(n) << std::endl;
	return 0;
}
