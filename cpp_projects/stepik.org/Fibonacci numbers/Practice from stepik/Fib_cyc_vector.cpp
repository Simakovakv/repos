/* Optimized Fibonacci sequence calls using a vector
time up to 40th num = 0,005s
time of 100000th num = 0,009s (contingently)
time of 1000000th num = 0,018s (contingently)
*/
#include <iostream>
#include <cassert>
#include <vector>

class Fibonacci final {
	public:
		static int get(int n) {
			assert(n >=0);
			
			static std::vector<int> cache;
			cache.resize(n + 1);
			cache[0] = 0;
			cache[1] = 1;		
			for (int i = 2; i<= n; i++) {
				cache[i] = cache[i - 2] + cache[i - 1];
			}
			return cache[n];
		}
};

int main(void) {
	int n;
	std::cin >> n;
	std::cout << Fibonacci::get(n) << std::endl;
	return 0;
}
