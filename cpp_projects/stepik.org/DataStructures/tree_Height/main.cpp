/* Вычислить высоту данного дерева.1 ≤ n ≤ 105
Вход. Корневое дерево с вершинами {0, . . . , n−1}, заданное
как последовательность parent0, . . . , parentn−1
, где parenti — родитель i-й вершины.
Первая строка содержит натуральное число n. Вторая
строка содержит n целых чисел parent0, . . . , parentn−1. Для
каждого 0 ≤ i ≤ n − 1, parenti — родитель вершины i; если
parenti = −1, то i является корнем. Гарантируется, что корень
ровно один. Гарантируется, что данная последовательность задаёт
дерево.
Выход. Высота дерева.
 *
*/
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

long findHeight(long top, vector <long>* nodes, long height, long *max)
{
    ++height;
    int i = 0;
    for (long n : *nodes)
    {
        if (top == n)
        {
            //height = max (height,findHeight(i, nodes, height));
            findHeight(i, nodes, height, max);
            if(height > *max )
                *max = height;
            //cout << i << " " << height << " " << *max << endl;
        }
        i++;
    }
    return height;
}



int main()
{
    long num = 0, node = 0, height = 0, max = 0;
    long top = -1;
    vector <long> nodes;
    cin >> num;
    for (int i = 0; i < num; i++)
    {
        cin >> node;
        nodes.push_back(node);
    }
    findHeight(top, &nodes, height, &max);
    cout << max << endl;
    return 0;
}
