/*
 * Проверить, правильно ли расставлены скобки в данном коде.
 * Input: Строка s[1 . . . n], состоящая из заглавных и прописных
букв латинского алфавита, цифр, знаков препинания и скобок
из множества []{}(). 1 ≤ n ≤ 105

Output: Если скобки в s расставлены правильно, выведите
строку “Success". В противном случае выведите индекс (используя
индексацию с единицы) первой закрывающей скобки, для
которой нет соответствующей открывающей. Если такой нет,
выведите индекс первой открывающей скобки, для которой нет
соответствующей закрывающей.
Вход:
[]
Выход:
Success
Вход:
{[}
Выход:
3
 */
#include <iostream>
#include <stack>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    stack <pair<char, int>> sStack, tStack;
    string text = "";
    cin >> text;
    int count = 1;
    for (char ch : text)
    {
        if (ch == '(' || ch == '[' || ch == '{')
        {
            sStack.push(make_pair(ch, count));
        }
        else if (!(sStack.empty()) && ((sStack.top().first == '(' && ch == ')') || (sStack.top().first == '[' && ch == ']') || (sStack.top().first == '{' && ch == '}')))
        {
            sStack.pop();
        }
        else if (ch == ']' || ch == ')' || ch == '}')
        {
            tStack.push(make_pair(ch, count));
            break;
        }
        ++count;
    }
    if (sStack.empty() && tStack.empty()) cout << "Success" << endl;
    else if (!tStack.empty()) cout << tStack.top().second << endl;
    else cout << sStack.top().second << endl;
    return 0;
}
