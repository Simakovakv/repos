/*Program to find GCD of 2 numbers using Euclidean algorithm

Sample input: 5 n 8, 64 n 256, 514229 832040, 420196140717  679891637638612258
Sample output: 1, 64, 1, 1 
time of 1st sample = 0,005s
time of 2nd sample = 0,005s
time of 3rd sample = 0,006s
time of 4th sample = 0,006s
*/
#include <iostream>
#include <cassert>
#include <cstdint>

template <class Int>
Int gcd(Int a, Int b) {
	assert(a > 0 && b > 0);
	
	while (a > 0 && b > 0) {
		if (a > b) {
			a %= b;
		}	 
		else {
			b %= a;
		}
	}
	return a == 0 ? b : a;
}

int main(void) {
	std::int64_t a, b;
	std::cin >> a >> b;
	std:: cout << gcd(a, b) << std::endl; 
}
