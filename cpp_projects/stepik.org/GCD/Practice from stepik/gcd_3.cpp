/*Optimized Program to find GCD of 2 numbers using Euclidean algorithm

Sample input: 5 n 8, 64 n 256, 514229 832040, 420196140717  679891637638612258
Sample output: 1, 64, 1, 1 
time 1st 4th sample = 0,005s
*/
#include <iostream>
#include <cassert>
#include <utility>
#include <cstdint>

template <class Int>
Int gcd(Int a, Int b) {
	assert(a > 0 && b > 0);
	
	while (b > 0) {
			a %= b;
			std::swap(a, b);
	}
	return a;
}

int main(void) {
	std::int64_t a, b;
	std::cin >> a >> b;
	std:: cout << gcd(a, b) << std::endl; 
}
