/*Program to find GCD of 2 numbers with optimization of 1 sample
D(f): a = d1*d2
min(d1, d2) <= sqrt(a)
max(d1, d2) >= sqrt(a)
d2 = a/d1
Sample input: 5 n 8, 64 n 256, 514229 n 832040, 420196140717  679891637638612258
Sample output: 1, 64, 1, 1 
time of 1st sample = 0,006s
time of 2nd sample = 0,005s
time of 3rd sample = 0,006s
time of 4th sample = 0,030s
*/
#include <iostream>
#include <cassert>
#include <cstdint>

template <class Int>
Int gcd(Int a, Int b) {
	assert(a > 0 && b > 0);
	
	Int current_gcd = 1;
	for (Int d = 1; d * d <= a && d <= b; d++) {
		if (a % d == 0) {
			if (b % d == 0) {
				current_gcd = d;
			}
			Int big_d = a / d;
			if (b % big_d == 0) {
				return big_d;
			}
		}
	}
	return current_gcd;
}

int main(void) {
	std::int64_t a, b;
	std::cin >> a >> b;
	std:: cout << gcd(a, b) << std::endl; 
}
