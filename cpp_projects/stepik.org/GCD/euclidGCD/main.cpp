#include <iostream>

using namespace std;

int64_t calcGCD(int64_t big, int64_t small)
{
    int64_t result = big % small;
    big = small;
    small = result;
    return small;
}
int main()
{
    int64_t a = 0, b = 0;
    bool key = true;
    //cout << "Enter a and b to find GCD" << endl;

    cin >> a >> b;
    while (key) {
        if (a == 0)
        {
            cout << b << endl;
            key = false;
        }
        else if (b == 0)
        {
            cout << a << endl;
            key = false;
        }
        else if (a >= b)
            a = calcGCD(a, b);
        else if (b >= a)
            b = calcGCD(b, a);
    }
    return 0;
}
