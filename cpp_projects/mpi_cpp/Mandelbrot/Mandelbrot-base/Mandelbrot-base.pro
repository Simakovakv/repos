QT += core
QT -= gui

TARGET = Mandelbrot-base
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

TEMPLATE = app

SOURCES += main.cpp

LIBS += -lopencv_highgui
LIBS += -lopencv_core
