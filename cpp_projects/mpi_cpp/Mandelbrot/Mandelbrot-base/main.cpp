#include <iostream>
#include <opencv2/highgui/highgui.hpp>

bool isMandelbrot(const std::complex<float>& c)
{
    const int iter_count = 50;
    const int R = 2;

    std::complex<float> z(0,0);
    for(int i=0; i<iter_count; i++)
    {
        z= z*z + c;

        if(std::abs(z) > R)
            return false;
    }

    return true;
}

int main()
{
    const cv::Size2f size_math(3, 2);
    const cv::Point2f base_point(-2, 1);

    cv::Size size_dots;

    std::cout << "enter a: ";
    std::cin >> size_dots.width;

    std::cout << "enter b: ";
    std::cin >> size_dots.height;

    cv::Size2f dot_res;
    dot_res.width = size_dots.width/size_math.width;
    dot_res.height = size_dots.height/size_math.height;

    cv::Mat mat(size_dots, CV_8U, cv::Scalar(UCHAR_MAX));

    for(int i =0; i<mat.rows; i++)
        for(int j=0; j<mat.cols; j++)
        {
            std::complex<float> val(base_point.x + j/dot_res.width, base_point.y - i/dot_res.height);

            if(isMandelbrot(val))
                mat.at<uchar>(i,j)=0;
        }

    cv::imshow("Mandelbrot", mat);
    cv::waitKey();
}
