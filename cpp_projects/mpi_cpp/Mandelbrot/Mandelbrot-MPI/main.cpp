#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <mpi/mpi.h>

bool isMandelbrot(const std::complex<float>& c)
{
    const int iter_count = 50;
    const int R = 2;

    std::complex<float> z(0,0);
    for(int i=0; i<iter_count; i++)
    {
        z= z*z + c;

        if(std::abs(z) > R)
            return false;
    }

    return true;
}

int main()
{
    MPI_Init(nullptr, nullptr);
    double starttime, endtime;
    starttime = MPI_Wtime();
    int count_proc;
    MPI_Comm_size(MPI_COMM_WORLD, &count_proc);

    int id_proc;
    MPI_Comm_rank(MPI_COMM_WORLD, &id_proc);

    const cv::Point2f base_point(-2, 1);
    cv::Size size_dots_i;
    cv::Size2f dot_res;

    if(id_proc == 0)
    {
        const cv::Size2f size_math(3, 2);

        cv::Size size_dots;

        std::cout << "enter a: ";
        std::cin >> size_dots.width;
//        size_dots.width = 900;
        std::cout << "enter b: ";
        std::cin >> size_dots.height;
//        size_dots.height = 600;
        dot_res.width = size_dots.width/size_math.width;
        dot_res.height = size_dots.height/size_math.height;

        size_dots_i.width = size_dots.width;
        size_dots_i.height = size_dots.height/count_proc;
    }

    MPI_Bcast(&size_dots_i, sizeof(size_dots_i), MPI_BYTE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dot_res, sizeof(dot_res), MPI_BYTE, 0, MPI_COMM_WORLD);

    cv::Mat mat(size_dots_i, CV_8U, cv::Scalar(UCHAR_MAX));

    for(int i =0; i<mat.rows; i++)
        for(int j=0; j<mat.cols; j++)
        {
            std::complex<float> val(base_point.x + j/dot_res.width, base_point.y - (i + id_proc * mat.rows)/dot_res.height);

            if(isMandelbrot(val))
                mat.at<uchar>(i,j)=0;
        }

    cv::Mat general;
    if(id_proc == 0)
    {
        general = cv::Mat(cv::Size(size_dots_i.width, size_dots_i.height * count_proc), CV_8U);
    }

    MPI_Gather(mat.data, mat.total() * sizeof(uchar), MPI_BYTE, general.data, mat.total() * sizeof(uchar), MPI_BYTE,
               0, MPI_COMM_WORLD);

    if(id_proc == 0)
    {
        cv::imshow("Mandelbrot", general);
        cv::waitKey();
    }
    //endtime = MPI_Wtime();
  // printf("Выполнение заняло %f секунд\n", endtime-starttime);
    MPI_Finalize();

}
