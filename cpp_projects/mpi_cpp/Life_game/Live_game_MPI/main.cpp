#include "livegame.h"

int main()
{
    MPI_Init(nullptr, nullptr);

    cart_param param = create_cart();

    print_param(param);

    cv::Mat general = create_general_mat(param);
    show_mat(param, "general", general, 0);

    cv::Mat submat = send_recv_general(param, general);

    std::deque<cv::Mat> history;
    while(check_game(param, history, general))
    {
        send_recv_bounds(param, submat);
        life_step(submat);
        update_general(param, general, submat);
        int key = show_mat(param, "general", general, 0);

        if(key == 27)
            break;
        else if(key == 's')
            cv::imwrite("/home/ksenia/src/Life_game/general.bmp", general);
    }

    MPI_Finalize();
    return 0;
}
