QT += core
QT -= gui
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lopencv_highgui
LIBS += -lopencv_core
LIBS += -lmpi

SOURCES += main.cpp \
    livegame.cpp

HEADERS += \
    livegame.h

