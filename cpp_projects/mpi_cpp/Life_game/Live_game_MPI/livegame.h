#pragma once

#include <opencv2/highgui/highgui.hpp>
#include <mpi/mpi.h>
#include <deque>

struct cart_param
{
    MPI_Comm comm;
    int wsize;
    cv::Size dims;
    int curr_rank;
    cv::Point curr_coord;
    int mat_side;
    int period;
    cv::Scalar not_init_val;
    size_t history_size;
};

cv::Point rank_to_coord(const cart_param& param, int rank);
int coord_to_rank(const cart_param& param, const cv::Point& point);

cart_param create_cart();
void print_param(const cart_param& param);
cv::Mat create_general_mat(const cart_param& param);
int show_mat(const cart_param& param, const std::string& name, cv::Mat mat, int rank, bool print_coord = false);
cv::Mat send_recv_general(const cart_param& param, cv::Mat general);
void send_recv_bounds(const cart_param& param, cv::Mat submat);
void life_step(cv::Mat submat);
void update_general(const cart_param& param, cv::Mat general, cv::Mat submat);
bool check_game(const cart_param& param, std::deque<cv::Mat>& history, cv::Mat general);
