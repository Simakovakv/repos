#include "livegame.h"
#include <iostream>

cv::Point rank_to_coord(const cart_param& param, int rank)
{
    int coord[2];
    MPI_Cart_coords(param.comm, rank, 2, coord);

    cv::Point point;
    point.x = coord[0];
    point.y = coord[1];
    return point;
}

int coord_to_rank(const cart_param& param, const cv::Point& point)
{
    int coord[2] = {point.x, point.y};
    int rank;
    MPI_Cart_rank(param.comm, coord, &rank);
    return rank;
}

cart_param create_cart()
{
    cart_param param;

    param.period = 1;
    int dims_buf[2] = {0, 0};
    int periods_buf[2] = {param.period, param.period};

    MPI_Comm_size(MPI_COMM_WORLD, &param.wsize);
    MPI_Dims_create(param.wsize, 2, dims_buf);
    MPI_Cart_create(MPI_COMM_WORLD, 2, dims_buf, periods_buf, 0, &param.comm);
    MPI_Comm_rank(param.comm, &param.curr_rank);
    param.curr_coord = rank_to_coord(param, param.curr_rank);
    param.mat_side = 10;

    param.dims.width = dims_buf[0];
    param.dims.height = dims_buf[1];

    param.not_init_val = cv::Scalar(127);
    param.history_size = 100;

    return param;
}

void print_param(const cart_param& param)
{
    if(param.curr_rank == 0)
    {
        std::cout << "wsize: " << param.wsize << std::endl;
        std::cout << "dims: " << param.dims.width << ", " << param.dims.height << std::endl;
        std::cout << "sub mat side: " << param.mat_side << std::endl;
    }
}

cv::Mat create_general_mat(const cart_param& param)
{
    cv::Mat general;

    if(param.curr_rank == 0)
    {
        general = cv::Mat(param.dims.height * param.mat_side, param.dims.width * param.mat_side,
                        CV_8U, cv::Scalar(UCHAR_MAX));
        std::cout << "general mat: " << general.cols << ", " << general.rows << std::endl;

        srand(time(nullptr));
        for(int y=0; y<general.rows; y++)
            for(int x=0; x<general.cols; x++)
                if(rand() % 2)
                    general.at<uchar>(y, x) = 0;
    }

    return general;
}

int show_mat(const cart_param& param, const std::string& name, cv::Mat mat, int rank, bool print_coord)
{
    if(rank == -1 || param.curr_rank == rank)
    {
        std::string win_name;
        if(print_coord)
        {
            win_name = name + " (" + std::to_string(param.curr_coord.x) + ", " +
                       std::to_string(param.curr_coord.y) + ")";
        }
        else
            win_name = name;

        cv::namedWindow(win_name, cv::WINDOW_NORMAL);
        cv::imshow(win_name, mat);
        return cv::waitKey();
    }

    return -1;
}

cv::Mat send_recv_general(const cart_param& param, cv::Mat general)
{
    std::vector<cv::Mat> send_mats;

    if(param.curr_rank == 0)
    {

        for(int dim_x=0; dim_x<param.dims.width; dim_x++)
                for(int dim_y=0; dim_y<param.dims.height; dim_y++)
                {
                    cv::Mat send_mat = general(cv::Rect(dim_x * param.mat_side, dim_y * param.mat_side,
                                                                 param.mat_side, param.mat_side)).clone();
                    send_mats.push_back(send_mat);

                    MPI_Request req;
                    MPI_Isend(send_mat.data, send_mat.total() * sizeof(uchar), MPI_BYTE,
                             coord_to_rank(param, cv::Point(dim_x, dim_y)), 0, param.comm, &req);
                }
    }

    cv::Mat submat(param.mat_side + 2, param.mat_side + 2, CV_8U, param.not_init_val);
    cv::Mat recv_mat(param.mat_side, param.mat_side, CV_8U, param.not_init_val);

    MPI_Recv(recv_mat.data, recv_mat.total() * sizeof(uchar), MPI_BYTE, 0, 0, param.comm, MPI_STATUS_IGNORE);

    recv_mat.copyTo(submat(cv::Rect(cv::Point(1,1), cv::Size(submat.cols - 2, submat.rows - 2))));

    return submat;
}

void send_recv_bounds(const cart_param& param, cv::Mat submat)
{
    std::vector<cv::Mat> send_boundes;

    for(int y_diff=-1; y_diff<=1; y_diff++)
        for(int x_diff=-1; x_diff<=1; x_diff++)
        {
            if(x_diff == 0 && y_diff == 0)
                continue;

            cv::Point idx;
            idx.x = param.curr_coord.x + x_diff;
            idx.y = param.curr_coord.y + y_diff;

            if(idx.x == -1 || idx.x == param.dims.width || idx.y == -1 || idx.y == param.dims.height)
            {
                if(param.period)
                {
                    if(idx.x == -1)
                        idx.x = param.dims.width - 1;
                    else if(idx.x == param.dims.width)
                        idx.x = 0;

                    if(idx.y == -1)
                        idx.y = param.dims.height - 1;
                    else if(idx.y == param.dims.height)
                        idx.y = 0;
                }
                else
                    continue;
            }

            cv::Mat send_bound;
            int abs_diff_idx = (y_diff + 1) * 3 + (x_diff + 1);
            switch(abs_diff_idx)
            {

            case 0: //up-left
                send_bound = submat(cv::Rect(cv::Point(1, 1), cv::Size(1, 1))).clone();
                break;

            case 1: //up
                send_bound = submat(cv::Rect(cv::Point(1, 1), cv::Size(param.mat_side, 1))).clone();
                break;

            case 2: //up-right
                send_bound = submat(cv::Rect(cv::Point(param.mat_side, 1), cv::Size(1, 1))).clone();
                break;

            case 3: //left
                send_bound = submat(cv::Rect(cv::Point(1, 1), cv::Size(1, param.mat_side))).clone();
                break;

            case 5: //right
                send_bound = submat(cv::Rect(cv::Point(param.mat_side, 1), cv::Size(1, param.mat_side))).clone();
                break;

            case 6: //bottom-left
                send_bound = submat(cv::Rect(cv::Point(1, param.mat_side), cv::Size(1, 1))).clone();
                break;

            case 7: //bottom
                send_bound = submat(cv::Rect(cv::Point(1, param.mat_side), cv::Size(param.mat_side, 1))).clone();
                break;

            case 8: //bottom-right
                send_bound = submat(cv::Rect(cv::Point(param.mat_side, param.mat_side), cv::Size(1, 1))).clone();
                break;
            }

            send_boundes.push_back(send_bound);

            MPI_Request req;
            MPI_Isend(send_bound.data, send_bound.total() * sizeof(uchar), MPI_BYTE, coord_to_rank(param, idx),
                      8 - abs_diff_idx, param.comm, &req);
        }

    MPI_Barrier(param.comm);

    int message_flag;
    while(1)
    {
        MPI_Status status;
        MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, param.comm, &message_flag, &status);

        if(!message_flag)
            break;

        cv::Mat recv_bound;

        if(status.MPI_TAG == 1 || status.MPI_TAG == 7) //horiz
            recv_bound = cv::Mat(1, param.mat_side, CV_8U, param.not_init_val);
        else if(status.MPI_TAG == 3 || status.MPI_TAG == 5) //vert
            recv_bound = cv::Mat(param.mat_side, 1, CV_8U, param.not_init_val);
        else //point
            recv_bound = cv::Mat(1, 1, CV_8U, param.not_init_val);

        MPI_Recv(recv_bound.data, recv_bound.total() * sizeof(uchar), MPI_BYTE, MPI_ANY_SOURCE,
                 MPI_ANY_TAG, param.comm, MPI_STATUS_IGNORE);

        switch(status.MPI_TAG)
        {

        case 0: //up-left
            submat.at<uchar>(0, 0) = recv_bound.at<uchar>(0,0);
            break;

        case 1: //up
            recv_bound.copyTo(submat(cv::Rect(cv::Point(1, 0), cv::Size(param.mat_side, 1))));
            break;

        case 2: //up-right
            submat.at<uchar>(0, submat.cols - 1) = recv_bound.at<uchar>(0,0);
            break;

        case 3: //left
            recv_bound.copyTo(submat(cv::Rect(cv::Point(0, 1), cv::Size(1, param.mat_side))));
            break;

        case 5: //right
            recv_bound.copyTo(submat(cv::Rect(cv::Point(param.mat_side + 1, 1), cv::Size(1, param.mat_side))));
            break;

        case 6: //bottom-left
            submat.at<uchar>(submat.rows - 1, 0) = recv_bound.at<uchar>(0,0);
            break;

        case 7: //bottom
            recv_bound.copyTo(submat(cv::Rect(cv::Point(1, param.mat_side + 1), cv::Size(param.mat_side, 1))));
            break;

        case 8: //bottom-right
            submat.at<uchar>(submat.rows - 1, submat.cols - 1) = recv_bound.at<uchar>(0,0);
            break;
        }
    }
}

void life_step(cv::Mat submat)
{
    std::vector<cv::Point> change_cells;

    for(int y=1; y<submat.rows - 1; y++)
        for(int x=1; x<submat.cols - 1; x++)
        {
            int count_live = 0;
            for(int y_diff=-1; y_diff<=1; y_diff++)
                for(int x_diff=-1; x_diff<=1; x_diff++)
                {
                    if(x_diff == 0 && y_diff == 0)
                        continue;

                    cv::Point idx;
                    idx.x = x + x_diff;
                    idx.y = y + y_diff;

                    if(submat.at<uchar>(idx.y, idx.x) == 0)
                        count_live++;
                }

            if(submat.at<uchar>(y, x) == 0)
            {
                if(count_live<2 || count_live>3)
                    change_cells.push_back(cv::Point(x, y));
            }
            else
            {
                if(count_live == 3)
                    change_cells.push_back(cv::Point(x, y));
            }
        }

    for(const cv::Point& cell : change_cells)
    {
        uchar& val = submat.at<uchar>(cell.y, cell.x);

        if(val == 0)
          val = UCHAR_MAX;
        else
            val = 0;
    }
}

void update_general(const cart_param& param, cv::Mat general, cv::Mat submat)
{
    cv::Mat recv_mat;
    if(param.curr_rank == 0)
        recv_mat = cv::Mat(general.size(), CV_8U, param.not_init_val);

    cv::Mat send_mat = submat(cv::Rect(cv::Point(1,1), cv::Size(param.mat_side, param.mat_side))).clone();

    MPI_Gather(send_mat.data, send_mat.total() * sizeof(uchar), MPI_BYTE,
               recv_mat.data, send_mat.total() * sizeof(uchar), MPI_BYTE, 0, param.comm);

    if(param.curr_rank == 0)
    {
        for(int i=0; i<param.wsize; i++)
        {
            cv::Mat mat(param.mat_side, param.mat_side, CV_8U, recv_mat.data + (param.mat_side * param.mat_side) * i);
            cv::Point coord = rank_to_coord(param, i);

            cv::Point point_insert;
            point_insert.x = coord.x * param.mat_side;
            point_insert.y = coord.y * param.mat_side;

            mat.copyTo(general(cv::Rect(point_insert, cv::Size(param.mat_side, param.mat_side))));
        }
    }
}

bool check_game(const cart_param& param, std::deque<cv::Mat>& history, cv::Mat general)
{
    if(param.curr_rank == 0)
    {
        for(const cv::Mat& mat : history)
        {
            cv::Mat diff = (mat != general);
            if(cv::countNonZero(diff) == 0)
                return false;
        }

        history.push_back(general.clone());
        if(history.size() == param.history_size + 1)
            history.pop_front();

        return true;
    }

    return true;
}
