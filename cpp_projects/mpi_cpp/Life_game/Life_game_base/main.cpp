#include "livegame.h"

int main()
{
    livegame game(10, false);
    const std::vector<cv::Point> live_cells = { {2,2}, {3,3}, {4,3}, {2,4}, {3,4} };
    game.set_live(live_cells);
    //game.set_random();

    bool stop_flag = true;
    bool skip_next = false;

    while(1)
    {
        skip_next = false;

        if(!stop_flag)
        {
            game.draw_board("STOP");
            break;
        }
        else
        {
            switch(game.draw_board())
            {
            case 27:
                return 0;
            case 's':
                game.save_state("/home/ksenia/src/Grid/Life_game/Life_game_base/livegame.json");
                skip_next = true;
                break;
            }
        }

        if(!skip_next)
            stop_flag = game.next_state();
    }

    return 0;
}
