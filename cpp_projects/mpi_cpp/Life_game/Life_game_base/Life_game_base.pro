TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++14

INCLUDEPATH += $$(HOME)/src/Grid/Life_game/Life_game_base/src

SOURCES += main.cpp \
    livegame.cpp

LIBS += -lopencv_highgui
LIBS += -lopencv_core
LIBS += -lmpi

HEADERS += \
    livegame.h
