#include "livegame.h"
#include <time.h>
#include <json.hpp>
#include <fstream>

livegame::livegame(int size_board, bool periodic) :
    m_size_board(size_board), m_periodic(periodic),
    m_play_board(size_board, std::vector<bool>(size_board, false))
{
    srand(time(nullptr));
}

void livegame::set_live(const std::vector<cv::Point>& live_cells)
{
    for(const cv::Point& cell : live_cells)
    {
        m_play_board[cell.y][cell.x] = true;

        m_live_cells.insert(to_absolute_index(cell));

        std::vector<cv::Point> neighbors_cells;
        get_neighbors(cell, neighbors_cells);
        for(const cv::Point& cell : neighbors_cells)
        {
            m_active_cells.insert(to_absolute_index(cell));
        }
    }
}

void livegame::set_random()
{
    for(int y=0; y<m_size_board; y++)
    {
        for(int x=0; x<m_size_board; x++)
        {
            m_play_board[y][x] = rand() % 2;

            if(m_play_board[y][x])
            {
                cv::Point cell(x, y);
                m_live_cells.insert(to_absolute_index(cell));

                std::vector<cv::Point> neighbors_cells;
                get_neighbors(cell, neighbors_cells);
                for(const cv::Point& cell : neighbors_cells)
                {
                    m_active_cells.insert(to_absolute_index(cell));
                }
            }
        }
    }
}

void livegame::draw_grid()
{
    for(int i=1; i<m_size_board; i++)
    {
        int x = lround(float(m_image.cols)*i/m_size_board);
        cv::line(m_image, cv::Point(x,0), cv::Point(x, m_image.rows), cv::Scalar(0));
    }

    for(int i=1; i<m_size_board; i++)
    {
        int y = lround(float(m_image.rows)*i/m_size_board);
        cv::line(m_image, cv::Point(0,y), cv::Point(m_image.cols, y), cv::Scalar(0));
    }
}

void livegame::draw_cell(const cv::Point& cell)
{
    cv::Point center;

    center.x = lround(float(m_image.cols) * (cell.x+0.5f) / m_size_board);
    center.y = lround(float(m_image.rows) * (cell.y+0.5f) / m_size_board);

    int radius = lround(float(m_image.cols) / m_size_board / 2 * 0.9f);

    cv::circle(m_image, center, radius, cv::Scalar(0), -1);
}

int livegame::draw_board(const std::string& text)
{
    m_image = cv::Mat(600, 600, CV_8U, UCHAR_MAX);
    draw_grid();

    for(int idx : m_live_cells)
    {
        draw_cell(to_cell(idx));
    }

    if(text.length())
        cv::putText(m_image, text, cv::Point(100, m_image.rows/2), cv::FONT_HERSHEY_SIMPLEX, 5, 125, 7);

    cv::imshow("play board", m_image);
    return cv::waitKey();
}

inline int livegame::get_periodic_index(int idx) const
{
    if(idx == -1)
        return m_size_board - 1;

    if(idx == m_size_board)
        return 0;

    return idx;
}

int livegame::get_neighbors(const cv::Point& cell, std::vector<cv::Point>& neighbors) const
{
    neighbors.clear();

    int count_live = 0;
    for(int y=0; y<=2; y++)
        for(int x=0; x<=2; x++)
        {
            int x_idx = cell.x-1 + x;
            int y_idx = cell.y-1 + y;

            if(m_periodic)
            {
                x_idx = get_periodic_index(x_idx);
                y_idx = get_periodic_index(y_idx);
            }
            else
            {
                if(x_idx == -1 || x_idx == m_size_board || y_idx == -1 || y_idx == m_size_board)
                    continue;
            }

            if(!(x==1 && y==1))
                if(m_play_board[y_idx][x_idx])
                    count_live++;

            neighbors.push_back(cv::Point(x_idx, y_idx));
        }
    return count_live;
}

bool livegame::next_state()
{
    std::string serial_state = serialize_live();
    m_state_history.push(serial_state);
    m_state_hashes.insert(serial_state);

    if(m_state_history.size() == m_history_size + 1)
    {
        m_state_hashes.erase(m_state_history.front());
        m_state_history.pop();
    }

    std::vector<cv::Point> change_cells;

    for(int absolute_idx : m_active_cells)
    {
        cv::Point cell = to_cell(absolute_idx);

        std::vector<cv::Point> neighbors_cells;
        int count_live = get_neighbors(cell, neighbors_cells);

        if(m_play_board[cell.y][cell.x])
        {
            if(count_live<2 || count_live>3)
                change_cells.push_back(cell);
        }
        else
        {
            if(count_live == 3)
                change_cells.push_back(cell);
        }
    }

    m_active_cells.clear();
    for(const cv::Point& cell : change_cells)
    {
        m_play_board[cell.y][cell.x]=!m_play_board[cell.y][cell.x];

        if(m_play_board[cell.y][cell.x])
            m_live_cells.insert(to_absolute_index(cell));
        else
            m_live_cells.erase(to_absolute_index(cell));

        std::vector<cv::Point> neighbors_cells;
        get_neighbors(cell, neighbors_cells);
        for(const cv::Point& cell : neighbors_cells)
        {
            m_active_cells.insert(to_absolute_index(cell));
        }
    }

    if(m_state_hashes.count(serialize_live()))
        return false;
    else
        return true;
}

inline int livegame::to_absolute_index(const cv::Point& cell) const
{
    return cell.y * m_size_board + cell.x;
}

inline cv::Point livegame::to_cell(int idx) const
{
    cv::Point ret_val;
    ret_val.x = idx % m_size_board;
    ret_val.y = idx / m_size_board;
    return ret_val;
}

std::string livegame::serialize_live() const
{
    std::string ret_str;
    for(int idx : m_live_cells)
    {
        ret_str += std::to_string(idx);
    }
    return ret_str;
}

void livegame::save_state(const std::string& filepath) const
{
    nlohmann::json out(m_live_cells);
    std::ofstream file(filepath);
    file << out.dump(4);
}























