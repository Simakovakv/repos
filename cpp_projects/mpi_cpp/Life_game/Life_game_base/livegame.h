#ifndef LIVEGAME
#define LIVEGAME
#pragma once

#include <vector>
#include <set>
#include <unordered_set>
#include <queue>
#include <opencv2/highgui/highgui.hpp>

class livegame
{

public:
    livegame(int size_board, bool periodic);
    void set_live(const std::vector<cv::Point>& live_cells);
    void set_random();
    int draw_board(const std::string& text = "");
    bool next_state();
    void save_state(const std::string& filepath) const;

private:
    const size_t m_history_size = 100;

    int m_size_board;
    bool m_periodic;
    std::vector< std::vector<bool> > m_play_board;
    std::unordered_set<int> m_active_cells;
    std::set<int> m_live_cells;
    std::queue<std::string> m_state_history;
    std::unordered_set<std::string> m_state_hashes;
    cv::Mat m_image;

    void draw_grid();
    void draw_cell(const cv::Point& cell);
    int get_periodic_index(int idx) const;
    int get_neighbors(const cv::Point& cell, std::vector<cv::Point>& neighbors) const;
    int to_absolute_index(const cv::Point& cell) const;
    cv::Point to_cell(int idx) const;
    std::string serialize_live() const;
};
#endif // LIVEGAME

