#include "gauss.h"
#include <iostream>
#include <math.h>

void input_mat(mat_type& mat)
{
    int n;
    std::cout << "enter n: ";
    std::cin >> n;

    std::cout << "enter matrix: " << std::endl;
    int count_elem = n * n;
    mat.clear();
    for(int i=0; i<count_elem; i++)
    {
        if(i % n == 0)
            mat.emplace_back();

        float val;
        std::cin >> val;
        mat.back().push_back(val);
    }

    std::cout << "enter b: ";
    for(int i=0; i<n; i++)
    {
        float val;
        std::cin >> val;
        mat[i].push_back(val);
    }
}

void print_mat(const mat_type& mat)
{
    for(const row_type& row : mat)
    {
        for(float val : row)
            std::cout << val << " ";
        std::cout << std::endl;
    }
}

void print_solution(const sol_type& solution)
{
    std::cout << "solution vector: ";
    for(float sol : solution)
        std::cout << sol << " ";
    std::cout << std::endl;
}

mat_type send_general_mat(const param_type& param, const mat_type& general)
{
    std::vector<float> sendbuf;

    if(param.curr_rank == 0)
    {
        for(const row_type& row : general)
            sendbuf.insert(sendbuf.end(), row.begin(), row.end());

        int send_size = sendbuf.size() / param.wsize;
        for(int i=0; i<param.wsize; i++)
        {
            MPI_Request req;
            MPI_Isend(sendbuf.data() + i * send_size, send_size * sizeof(float), MPI_BYTE, i, 0, MPI_COMM_WORLD, &req);
        }
    }

    std::vector<float> recvbuf(param.gen_count_row * param.row_size / param.wsize);
    MPI_Recv(recvbuf.data(), recvbuf.size() * sizeof(float), MPI_BYTE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    mat_type submat;

    for(size_t i=0; i<recvbuf.size(); i++)
    {
        if(i % param.row_size == 0)
            submat.emplace_back();

        submat.back().push_back(recvbuf[i]);
    }

    return submat;
}

void forward_pass(const param_type& param, mat_type& submat)
{
    for(int idx_main_row = 0; idx_main_row < param.gen_count_row - 1; idx_main_row++)
    {
        swap_rows(param, submat, idx_main_row);

        int idx_main_col = idx_main_row;
        int rank = idx_main_row / param.wsize;
        int idx_sub_row = idx_main_row % param.wsize;

        row_type main_row;
        if(param.curr_rank == rank)
            main_row.insert(main_row.end(), submat[idx_sub_row].begin(), submat[idx_sub_row].end());
        else
            main_row.resize(param.row_size);

        MPI_Bcast(main_row.data(), main_row.size() * sizeof(float), MPI_BYTE, rank, MPI_COMM_WORLD);

        int idx_mod_start = std::max((idx_main_row + 1) - (int(submat.size()) * param.curr_rank), 0);
        for(int idx_mod_row = idx_mod_start; idx_mod_row < int(submat.size()); idx_mod_row++)
        {
            float coef = submat[idx_mod_row][idx_main_col] / main_row[idx_main_col] * -1;

            for(int idx_mod_col = idx_main_col; idx_mod_col < int(submat[idx_mod_row].size()); idx_mod_col++)
                submat[idx_mod_row][idx_mod_col] = coef * main_row[idx_mod_col] + submat[idx_mod_row][idx_mod_col];
        }
    }
}

sol_type backward_pass(const param_type& param, mat_type& submat)
{
    sol_type solution;
    for(int idx_main_row = param.gen_count_row - 1; idx_main_row >= 0; idx_main_row--)
    {
        int idx_main_col = idx_main_row;
        int rank = idx_main_row / param.wsize;
        int idx_sub_row = idx_main_row % param.wsize;

        float sol = 0;
        if(param.curr_rank == rank)
        {
            float b_val = submat[idx_sub_row].back();

            for(int idx_diff_col = idx_main_col + 1; idx_diff_col < int(submat[idx_sub_row].size()) - 1; idx_diff_col++)
                        b_val -= submat[idx_sub_row][idx_diff_col];

            sol = b_val / submat[idx_sub_row][idx_main_col];
        }

        MPI_Bcast(&sol, sizeof(float), MPI_BYTE, rank, MPI_COMM_WORLD);

        if(param.curr_rank == 0)
            solution.push_front(sol);

        int idx_prop_start = std::min((idx_main_row - 1) - (int(submat.size()) * param.curr_rank), int(submat.size()) - 1);
        for(int idx_prop_row = idx_prop_start; idx_prop_row >= 0; idx_prop_row--)
            submat[idx_prop_row][idx_main_col] *= sol;
    }

    return solution;
}

void swap_rows(const param_type& param, mat_type& submat, int idx_main_row)
{
    struct elem_type
    {
        float val;
        int subrow;
        int rank;
    };

    int idx_main_col = idx_main_row;
    int repl_rank = idx_main_row / param.wsize;
    int repl_sub_row = idx_main_row % param.wsize;

    int idx_substart_row = std::max(idx_main_row - (int(submat.size()) * param.curr_rank), 0);

    elem_type submax_elem = {0, -1, -1};
    for(int idx_max_row = idx_substart_row; idx_max_row < int(submat.size()); idx_max_row++)
    {
        float curr_val = fabs(submat[idx_max_row][idx_main_col]);
        if(curr_val > submax_elem.val)
        {
            submax_elem.val = curr_val;
            submax_elem.subrow = idx_max_row;
            submax_elem.rank = param.curr_rank;
        }
    }

    std::vector<elem_type> elems;
    if(param.curr_rank == 0)
        elems.resize(param.wsize);

    MPI_Gather(&submax_elem, sizeof(submax_elem), MPI_BYTE, elems.data(), sizeof(submax_elem),
               MPI_BYTE, 0, MPI_COMM_WORLD);

    elem_type genmax_elem = {0, -1, -1};
    if(param.curr_rank == 0)
    {
        for(const elem_type& elem : elems)
            if(elem.val > genmax_elem.val)
                genmax_elem.val = elem.val;
    }

    MPI_Bcast(&genmax_elem, sizeof(genmax_elem), MPI_BYTE, 0, MPI_COMM_WORLD);

    std::vector<float> recvbuf_repl;
    if(param.curr_rank == repl_rank)
    {
        MPI_Request req;
        MPI_Isend(submat[repl_sub_row].data(), submat[repl_sub_row].size() * sizeof(float), MPI_BYTE,
                  genmax_elem.rank, 0, MPI_COMM_WORLD, &req);

        recvbuf_repl.resize(param.row_size);
        MPI_Recv(recvbuf_repl.data(), recvbuf_repl.size() * sizeof(float), MPI_BYTE, genmax_elem.rank,
                 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    std::vector<float> recvbuf_max;
    if(param.curr_rank == genmax_elem.rank)
    {
        MPI_Request req;
        MPI_Isend(submat[genmax_elem.subrow].data(), submat[genmax_elem.subrow].size() * sizeof(float), MPI_BYTE,
                  repl_rank, 0, MPI_COMM_WORLD, &req);

        recvbuf_max.resize(param.row_size);
        MPI_Recv(recvbuf_max.data(), recvbuf_max.size() * sizeof(float), MPI_BYTE, repl_rank,
                 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    MPI_Barrier(MPI_COMM_WORLD);

    if(param.curr_rank == repl_rank)
        submat[repl_sub_row] = recvbuf_repl;

    if(param.curr_rank == genmax_elem.rank)
        submat[genmax_elem.subrow] = recvbuf_max;

    MPI_Barrier(MPI_COMM_WORLD);
}








































