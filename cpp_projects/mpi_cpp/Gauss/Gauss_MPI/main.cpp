#include "gauss.h"
#include <iostream>

int main()
{
    MPI_Init(nullptr, nullptr);
    param_type param;

    MPI_Comm_size(MPI_COMM_WORLD, &param.wsize);
    MPI_Comm_rank(MPI_COMM_WORLD, &param.curr_rank);

    mat_type general;

    if(param.curr_rank == 0)
    {
        input_mat(general);
        param.gen_count_row = general.size();
        param.row_size = general.front().size();
    }

    MPI_Bcast(&param.gen_count_row, sizeof(param.gen_count_row), MPI_BYTE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&param.row_size, sizeof(param.row_size), MPI_BYTE, 0, MPI_COMM_WORLD);

    mat_type submat = send_general_mat(param, general);
    forward_pass(param, submat);
    sol_type solution = backward_pass(param, submat);

    if(param.curr_rank == 0)
        print_solution(solution);

    MPI_Finalize();
    return 0;
}
