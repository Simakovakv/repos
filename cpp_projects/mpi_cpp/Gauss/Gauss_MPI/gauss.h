#pragma once

#include <vector>
#include <deque>
#include <mpi/mpi.h>

typedef std::vector<float> row_type;
typedef std::vector<row_type> mat_type;
typedef std::deque<float> sol_type;

struct param_type
{
    int wsize;
    int curr_rank;
    int row_size;
    int gen_count_row;
};

void input_mat(mat_type& mat);
void print_mat(const mat_type& mat);
void print_solution(const sol_type& solution);
mat_type send_general_mat(const param_type& param, const mat_type& general);
void forward_pass(const param_type& param, mat_type& submat);
sol_type backward_pass(const param_type& param, mat_type& submat);
void swap_rows(const param_type& param, mat_type& submat, int idx_main_row);
