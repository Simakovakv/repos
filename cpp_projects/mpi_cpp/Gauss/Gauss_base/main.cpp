#include "gauss.h"

int main()
{
    mat_type mat = {{1, -1, 3, 1, 5},
                    {4, -1, 5, 4, 4},
                    {2, -2, 4, 1, 6},
                    {1, -4, 5, -1, 3}};

    //3mat_type mat;
    //input_mat(mat);

    print_mat(mat);

    sol_type solution;
    forward_pass(mat);
    backward_pass(mat, solution);
    print_solution(solution);

    return 0;
}
