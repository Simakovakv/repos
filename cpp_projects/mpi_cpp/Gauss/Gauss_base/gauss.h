#pragma once

#include <vector>
#include <deque>

typedef std::vector<float> row_type;
typedef std::vector<row_type> mat_type;
typedef std::deque<float> sol_type;

void input_mat(mat_type& mat);
void print_solution(const sol_type& solution);
void print_mat(const mat_type& mat);
void forward_pass(mat_type& mat);
void backward_pass(mat_type& mat, sol_type& solution);
