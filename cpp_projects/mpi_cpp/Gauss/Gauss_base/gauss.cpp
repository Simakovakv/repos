#include "gauss.h"
#include <iostream>
#include <math.h>

void input_mat(mat_type& mat)
{
    int n;
    std::cout << "enter n: ";
    std::cin >> n;

    std::cout << "enter matrix: " << std::endl;
    int count_elem = n * n;
    mat.clear();
    for(int i=0; i<count_elem; i++)
    {
        if(i % n == 0)
            mat.emplace_back();

        float val;
        std::cin >> val;
        mat.back().push_back(val);
    }

    std::cout << "enter b: ";
    for(int i=0; i<n; i++)
    {
        float val;
        std::cin >> val;
        mat[i].push_back(val);
    }
}

void print_solution(const sol_type& solution)
{
    std::cout << "solution vector: ";
    for(float sol : solution)
        std::cout << sol << " ";
}

void print_mat(const mat_type& mat)
{
    for(const row_type& row : mat)
    {
        for(float val : row)
            std::cout << val << " ";
        std::cout << std::endl;
    }
}

void forward_pass(mat_type& mat)
{
    for(int idx_curr_row = 0; idx_curr_row < int(mat.size()) - 1; idx_curr_row++)
    {
        int idx_curr_col = idx_curr_row;

        {
            float max_val = 0;
            int max_row = 0;
            for(int idx_max_row = idx_curr_row; idx_max_row < int(mat.size()); idx_max_row++)
            {
                float curr_val = fabs(mat[idx_max_row][idx_curr_col]);
                if(curr_val > max_val)
                {
                    max_val = curr_val;
                    max_row = idx_max_row;
                }
            }

            if(idx_curr_row != max_row)
                std::swap(mat[idx_curr_row], mat[max_row]);
        }

        for(int idx_mod_row = idx_curr_row + 1; idx_mod_row < int(mat.size()); idx_mod_row++)
        {
            float coef = mat[idx_mod_row][idx_curr_col] / mat[idx_curr_row][idx_curr_col] * -1;

            for(int idx_mod_col = idx_curr_col; idx_mod_col < int(mat[idx_mod_row].size()); idx_mod_col++)
                mat[idx_mod_row][idx_mod_col] = coef * mat[idx_curr_row][idx_mod_col] + mat[idx_mod_row][idx_mod_col];
        }
    }
}

void backward_pass(mat_type& mat, sol_type& solution)
{
    for(int idx_curr_row = mat.size() - 1; idx_curr_row >= 0; idx_curr_row--)
    {
        int idx_curr_col = idx_curr_row;
        float b_val = mat[idx_curr_row].back();

        for(int idx_diff_col = idx_curr_col + 1; idx_diff_col < int(mat[idx_curr_row].size()) - 1; idx_diff_col++)
            b_val -= mat[idx_curr_row][idx_diff_col];

        float sol = b_val / mat[idx_curr_row][idx_curr_col];
        solution.push_front(sol);

        for(int idx_prop_row = idx_curr_row - 1; idx_prop_row >= 0; idx_prop_row--)
            mat[idx_prop_row][idx_curr_col] *= sol;
    }
}












































