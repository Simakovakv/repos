#include <vector>
#include <iostream>
#include <math.h>
#include <opencv2/highgui/highgui.hpp>

typedef std::vector<float> row_type;
typedef std::vector<row_type> grid_type;

void print_grid(const grid_type& grid)
{
    for(const row_type& row : grid)
    {
        for(float val : row)
            std::cout << val << " ";
        std::cout << std::endl;
    }
}

void visual_grid(const grid_type& grid)
{
    for(const row_type& row : grid)
    {
        cv::Mat frame(600, 600, CV_8U, cv::Scalar(UCHAR_MAX));

        for(size_t i=0; i<row.size(); i++)
        {
            cv::Point point;
            point.x = lround(i * (float(frame.cols) / (row.size() - 1)));
            point.y = lround((row[i] * frame.rows/3) * -1 + frame.rows/2);

            frame.at<uchar>(point.y, point.x) = 0;
        }

        cv::imshow("vibration", frame);
        cv::waitKey(10);
    }
}

int main()
{
    const float alpha = 2;

    const float all_time = 1.2f;
    const float all_length = 1;

    const int m_time = 2000;
    const int n_length = 100;

    const float step_time = all_time / (m_time - 1);
    const float step_length = all_length / (n_length - 1);

    grid_type grid(m_time, row_type(n_length));

    //set boundes conditions
    for(row_type& row : grid)
        row.front() = row.back() = 0;

    //set init conditions time 0
    for(size_t i=1; i < grid[0].size() - 1; i++)
        grid[0][i] = sin(M_PI * i * step_length);

    //set init conditions time 1
    for(size_t i=1; i < grid[1].size() - 1; i++)
        grid[1][i] = grid[0][i] + (step_time * step_time / 2) * (grid[0][i+1] - 2*grid[0][i] + grid[0][i-1]) /
                                                                (step_length * step_length);

    //main calc
    for(size_t j=2; j < grid.size(); j++)
        for(size_t i=1; i < grid[j].size() - 1; i++)
            grid[j][i] = 2*grid[j-1][i] - grid[j-2][i] + pow(alpha*step_time/step_length, 2) *
                                                        (grid[j-1][i+1] - 2*grid[j-1][i] + grid[j-1][i-1]);

    //print_grid(grid);
    visual_grid(grid);

    return 0;
}































