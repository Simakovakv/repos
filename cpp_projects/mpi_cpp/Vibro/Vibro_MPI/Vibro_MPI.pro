TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    vibro.cpp \
    vibro.cpp

LIBS += -lopencv_core
LIBS += -lopencv_highgui
LIBS += -lmpi

HEADERS += \
    vibro.h \
    vibro.h
