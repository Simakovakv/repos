#include "vibro.h"
#include <iostream>
#include <opencv2/highgui/highgui.hpp>

void print_grid(const grid_type& grid)
{
    for(const row_type& row : grid)
    {
        for(float val : row)
            std::cout << val << " ";
        std::cout << std::endl;
    }
}

void visual_grid(const grid_type& grid)
{
    for(const row_type& row : grid)
    {
        cv::Mat frame(600, 600, CV_8U, cv::Scalar(UCHAR_MAX));

        for(size_t i=0; i<row.size(); i++)
        {
            cv::Point point;
            point.x = lround(i * (float(frame.cols) / (row.size() - 1)));
            point.y = lround((row[i] * frame.rows/3) * -1 + frame.rows/2);

            frame.at<uchar>(point.y, point.x) = 0;
        }

        cv::imshow("vibration", frame);
        cv::waitKey(10);
    }
}

grid_type gather_general_grid(const param_type& param, const grid_type& subgrid)
{
    std::vector<float> sendbuf;

    for(const row_type& row : subgrid)
        sendbuf.insert(sendbuf.end(), row.begin(), row.end());

    std::vector<float> recvbuf;
    if(param.curr_rank == 0)
        recvbuf.resize(sendbuf.size() * param.wsize);

    MPI_Gather(sendbuf.data(), sendbuf.size() * sizeof(float), MPI_BYTE, recvbuf.data(),
               sendbuf.size() * sizeof(float), MPI_BYTE, 0, MPI_COMM_WORLD);

    grid_type general;

    if(param.curr_rank == 0)
    {
        general.resize(subgrid.size());

        for(int i=0, abs_idx=0; i<param.wsize; i++)
            for(row_type& row : general)
                for(size_t i=0; i<subgrid[0].size(); i++, abs_idx++)
                    row.push_back(recvbuf[abs_idx]);
    }

    return general;
}

boundes_type get_boundes(const param_type& param, const grid_type& subgrid, int idx_curr_row)
{
    MPI_Request req;

    if(param.curr_rank != 0)
        MPI_Isend(&subgrid[idx_curr_row].front(), sizeof(float), MPI_BYTE, param.curr_rank - 1, 0, MPI_COMM_WORLD, &req);

    if(param.curr_rank != param.wsize - 1)
        MPI_Isend(&subgrid[idx_curr_row].back(), sizeof(float), MPI_BYTE, param.curr_rank + 1, 0, MPI_COMM_WORLD, &req);

    boundes_type boundes = {0, 0};

    if(param.curr_rank != 0)
        MPI_Recv(&boundes.left, sizeof(float), MPI_BYTE, param.curr_rank - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    if(param.curr_rank != param.wsize - 1)
        MPI_Recv(&boundes.right, sizeof(float), MPI_BYTE, param.curr_rank + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    MPI_Barrier(MPI_COMM_WORLD);

    return boundes;
}








































