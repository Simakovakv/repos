#pragma once

#include <vector>
#include <mpi/mpi.h>

typedef std::vector<float> row_type;
typedef std::vector<row_type> grid_type;

struct param_type
{
    int wsize;
    int curr_rank;
};

struct boundes_type
{
    float left;
    float right;
};

void print_grid(const grid_type& grid);
void visual_grid(const grid_type& grid);
grid_type gather_general_grid(const param_type& param, const grid_type& subgrid);
boundes_type get_boundes(const param_type& param, const grid_type& subgrid, int idx_curr_row);
