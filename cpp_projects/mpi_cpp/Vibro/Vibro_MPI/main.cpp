#include "vibro.h"
#include <math.h>

int main()
{
    MPI_Init(nullptr, nullptr);

    param_type param;
    MPI_Comm_size(MPI_COMM_WORLD, &param.wsize);
    MPI_Comm_rank(MPI_COMM_WORLD, &param.curr_rank);

    const float alpha = 2;

    const float all_time = 0.06f;
    const float all_length = 1;

    const int m_time = 16;
    const int n_length = 16;

    const float step_time = all_time / (m_time - 1);
    const float step_length = all_length / (n_length - 1);

    grid_type subgrid(m_time, row_type(n_length / param.wsize));

    //set init conditions time 0
    for(size_t i=0; i < subgrid[0].size(); i++)
        subgrid[0][i] = sin(M_PI * (param.curr_rank * subgrid[0].size() + i) * step_length);

    boundes_type boundes = get_boundes(param, subgrid, 0);

    //set init conditions time 1
    for(size_t i=0; i < subgrid[1].size(); i++)
    {
        if(param.curr_rank == 0)
            if(i == 0)
                continue;

        if(param.curr_rank == param.wsize - 1)
            if(i == subgrid[1].size() - 1)
                continue;

        float left;
        if(i == 0)
            left = boundes.left;
        else
            left = subgrid[0][i-1];

        float right;
        if(i == subgrid[1].size() - 1)
            right = boundes.right;
        else
            right = subgrid[0][i+1];

        subgrid[1][i] = subgrid[0][i] + (step_time * step_time / 2) * (right - 2*subgrid[0][i] + left) /
                                                                      (step_length * step_length);
    }

    //set boundes conditions left
    if(param.curr_rank == 0)
        for(row_type& row : subgrid)
            row.front() = 0;

    //set boundes conditions right
    if(param.curr_rank == param.wsize - 1)
        for(row_type& row : subgrid)
            row.back() = 0;

    MPI_Barrier(MPI_COMM_WORLD);

    //main calc
    for(size_t j=2; j < subgrid.size(); j++)
    {
        boundes_type boundes = get_boundes(param, subgrid, j - 1);

        for(size_t i=0; i < subgrid[j].size(); i++)
        {
            if(param.curr_rank == 0)
                if(i == 0)
                    continue;

            if(param.curr_rank == param.wsize - 1)
                if(i == subgrid[1].size() - 1)
                    continue;

            float left;
            if(i == 0)
                left = boundes.left;
            else
                left = subgrid[0][i-1];

            float right;
            if(i == subgrid[1].size() - 1)
                right = boundes.right;
            else
                right = subgrid[0][i+1];

            subgrid[j][i] = 2*subgrid[j-1][i] - subgrid[j-2][i] + pow(alpha*step_time/step_length, 2) *
                                                            (right - 2*subgrid[j-1][i] + left);
        }

        MPI_Barrier(MPI_COMM_WORLD);
    }

    grid_type general = gather_general_grid(param, subgrid);

    if(param.curr_rank == 0)
        print_grid(general);
        //visual_grid(grid);

    MPI_Finalize();
    return 0;
}






























