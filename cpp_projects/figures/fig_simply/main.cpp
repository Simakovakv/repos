// Example program
#include <QCoreApplication>
#include <iostream>
#include <string>
#include <math.h>
#include <cstdlib>

using namespace std;

string **arrPtr = nullptr;

void takeResults(float p, double s, float p2, string name, int num) {
    arrPtr[num-1][0]= to_string(num);
    arrPtr[num-1][1]= name;
    arrPtr[num-1][2]= "S = " + to_string(s);
    arrPtr[num-1][3]= "P = " + to_string(p);
    arrPtr[num-1][4]= "P2 = " + to_string(p2);

//    cout << num << " - ";
//    cout << "Name: " << name << endl;
//    cout << "P = " << p << endl;
//    cout << "S = " << s << endl;
}

void goTriangle (int a, int b, int c, int num) {
    cout << "triangle's parameters are " << a << " " << b << " " << c << endl;
    float per, geron;
    float sqr;
    string name = "triangle";
    per = a + b + c;
    geron = per / 2 * (per / 2 - a) * (per / 2 - b) * (per / 2 - c);
    sqr = sqrt(geron);
    float p2 = per * 2;
    takeResults(per, sqr, p2 , name, num);
    //cout << "Square of triangle = " << sqrt(geron) << endl;
    //cout << "Perimeter of tringle = " << per << endl;
}
void goSquare (int s, int num) {
    cout << "square's parameters are " << s << " " << s << " " << s << " " << s << endl;
    float sqr, per;
    string name = "square";
    per = 4* s;
    sqr = s * s;
    float p2 = per * 2;
    takeResults(per, sqr, p2, name, num);
    //cout << "Perimeter of circle = " << per << endl;
    //cout << "Square of circle = " << sqr << endl;
}

void goCircle (int r, int num) {
    cout << "circle's parameter is " << r << " " << endl;
    float sqr, per;
    string name = "Circle";
    //const double pi = 3.14159265;
    per = r * 2 * M_PI;
    sqr = M_PI * r * r;
    float p2 = per * 2;
    takeResults(per, sqr, p2, name, num);
    //cout << "Perimeter of circle = " << per << endl;
   //cout << "Square of circle = " << sqr << endl;
}

void goEllipse (int a1, int a2, int num) {
    cout << "ellipse's parameters are " << a1 << " " << a2 << endl;
    double sqr, per;
    string name = "Ellipse";
    //const double pi = 3.14159265;
    per = 4 * ((M_PI * a1 / 2 * a2 / 2 + (a1 / 2 - a2 / 2)) / (a1 / 2 + a2 / 2));
    sqr = M_PI * a1/2 * a2/2;
    float p2 = per * 2;
    takeResults(per, sqr, p2, name, num);
    //cout << "Perimeter of ellipse = " << per << endl;
    //cout << "Square of ellipse = " << sqr << endl;
}

void goRectangle (int a, int b, int num) {
    cout << "triangle's parameters are " << a << " " << b << " " << a << " " << b << endl;
    float sqr, per;
    string name = "Rectangle";
    per = 2 * (a + b);
    sqr = a * b;
    float p2 = per * 2;
    takeResults(per, sqr, p2, name, num);
    //cout << "Perimeter of rectangle = " << per << endl;
    //cout << "Square of rectangle = " << sqr << endl;
}

void setFigure(int num) {
    int choice;
    cout << "go set figure number " << num << endl;
    cout << "What kind Of figure is? (1 - triangle, 2 -square, 3 - circle, 4 - ellipse, 5 - rectangle) " << endl;
    cin >> choice;
    switch(choice)
    {
        case 1:
        {
            cout << "your choice N " << num << " " << "is triangle" << endl;
            int fside, sside, thside;
            cout << "Set 1st side: ";
            cin >> fside;
            cout << "Set 2nd side: ";
            cin >> sside;
            cout << "Set 3rd side: ";
            cin >> thside;
            goTriangle(fside, sside, thside, num);
           // return sqr, per;
            break;
        }

        case 2:     {
            cout << "your choice N " << num << "is square" << endl;
            int side;
            cout << "Set side: ";
            cin >> side;
            goSquare(side, num);
            //return sqr, per;
             break;
        }

        case 3:     {
            cout << "your choice N " << num << "is circle" << endl;
            int radius;
            cout << "Set radius: ";
            cin >> radius;
            goCircle(radius, num);
           // return sqr, per;
            break;
        }

        case 4:     {
            cout << "your choice N " << num << "is ellipse" << endl;
            int axis1, axis2;
            cout << "Set axis1: ";
            cin >> axis1;
            cout << "Set axis2: ";
            cin >> axis2;
            goEllipse(axis1, axis2, num);
            //return sqr, per;
            break;
        }

        case 5:     {
            cout << "your choice N " << num << "is rectangle" << endl;
            int fside, sside;
            cout << "Set 1st side: ";
            cin >> fside;
            cout << "Set 2nd side: ";
            cin >> sside;
            goRectangle(fside, sside, num);
            //return sqr, per;
            break;
        }

        default:     {
            cout << "Please try again" << endl;
            break;
        }

    }
}

void go(int N) {
    cout << endl << "go work with " << N << " figures" << endl;
    for (int i = 1; i <= N; i++) {
        setFigure(i);
    }
}

int main() {
    int count;
    cout << "Enter count of figures: ";
    cin >> count;
    arrPtr = new string* [count];
    for (int i = 0; i < count; i++)
        arrPtr[i] = new string [5];
    go(count);
    for (int i = 0; i < count; ++i)
    {
        for (int j = 0; j < 5; ++j)
        {
            cout << arrPtr[i][j] << ' ';
        }
        cout << endl;
    }

    for (int i = 0; i < count; i++)
        delete[] arrPtr[i];
    delete[] arrPtr;
    arrPtr = nullptr;

    return 0;
}

