#ifndef CHILDCLASSES_H
#define CHILDCLASSES_H
#include <figure.h>

class Triangle:public Figure
{
public:
    Triangle(double a, double b, double c);
    ~Triangle() override;
   //virtual float get_per2() override;
};

class Square:public Figure
{
public:
    Square(double a);
    ~Square() override;
};

class Circle:public Figure
{
public:
    Circle(double r);
    ~Circle() override;
};

class Ellipse:public Figure
{
public:
    Ellipse(double a1, double a2);
    ~Ellipse() override;
};

class Rectangle:public Figure
{
public:
    Rectangle(double a, double b);
    ~Rectangle() override;
};
#endif // CHILDCLASSES_H
