#include "childclasses.h"
#include "figure.h"
#include <QCoreApplication>
#include <iostream>
#include <string>
#include <math.h>
#include <cstdlib>

using namespace std;

Triangle::Triangle(double a, double b, double c)
{
    name = "Triangle";
    per = a + b + c;
    double geron = per / 2 * (per / 2 - a) * (per / 2 - b) * (per / 2 - c);
    sqr = sqrt(geron);
    cout << "construct Triangle" << endl;
}

Triangle::~Triangle()
{
    cout << "destroy Triangle" << endl;
}

Square::Square(double a)
{
    name = "Square";
    per = a * 4;
    sqr = a * a;
    cout << "construct Square" << endl;
}

Square::~Square()
{
    cout << "destroy Square" << endl;
}

Circle::Circle(double r)
{
    name = "Circle";
    per = 2 * M_PI * r;
    sqr = M_PI * r * r;
    cout << "construct Circle" << endl;
}

Circle::~Circle()
{
    cout << "destroy Circle" << endl;
}

Ellipse::Ellipse(double a1, double a2)
{
    name = "Ellipse";
    per = 4 * ((M_PI * a1 / 2 * a2 / 2 + (a1 / 2 - a2 / 2)) / (a1 / 2 + a2 / 2));
    sqr = M_PI * a1/2 * a2/2;
    cout << "construct Ellipse" << endl;
}

Ellipse::~Ellipse()
{
    cout << "destroy Ellipse" << endl;
}

Rectangle::Rectangle(double a, double b)
{
    name = "Rectangle";
    per = 2 * (a + b);
    sqr = a * b;
    cout << "construct Rectangle" << endl;
}

Rectangle::~Rectangle()
{
    cout << "destroy Rectangle" << endl;
}
