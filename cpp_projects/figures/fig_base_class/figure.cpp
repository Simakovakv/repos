#include "figure.h"
#include <QCoreApplication>
#include <iostream>
#include <string>
#include <math.h>
#include <cstdlib>

using namespace std;

Figure::Figure()
{
    cout << "construct Figure" << endl;
}

double Figure::get_per2() {
    p2 = per * 2;
    //cout << "call get_per2" << endl;
    return p2;
}

double Figure::get_per() {
    return per;
}

double Figure::get_sqr() {
    return sqr;
}

Figure::~Figure()
{
    cout << "destroy Figure" << endl;
}

