#include <QCoreApplication>
#include <iostream>
#include <string>
#include <math.h>
#include <cstdlib>
#include <vector>
//#include <figure.h>
#include <childclasses.h>

//#pragma GCC diagnostic ignored "-Wpadded"

using namespace std;
Figure* setFigure(int num) {
    int choice;
    cout << "go set figure number " << num << endl;
    cout << "What kind Of figure is? (1 - triangle, 2 -square, 3 - circle, 4 - ellipse, 5 - rectangle) " << endl;
    cin >> choice;
    switch(choice)
    {
    case 1:
    {
        cout << "your choice N " << num << " " << "is triangle" << endl;
        int fside, sside, thside;
        cout << "Set 1st side: ";
        cin >> fside;
        cout << "Set 2nd side: ";
        cin >> sside;
        cout << "Set 3rd side: ";
        cin >> thside;
        return new Triangle(fside, sside, thside);
        //break;
    }

    case 2:     {
        cout << "your choice N " << num << "is square" << endl;
        int side;
        cout << "Set side: ";
        cin >> side;
        return new Square(side);
        //break;
    }

    case 3:     {
        cout << "your choice N " << num << "is circle" << endl;
        int radius;
        cout << "Set radius: ";
        cin >> radius;
        return new Circle(radius);
        // break;
    }

    case 4:     {
        cout << "your choice N " << num << "is ellipse" << endl;
        int axis1, axis2;
        cout << "Set axis1: ";
        cin >> axis1;
        cout << "Set axis2: ";
        cin >> axis2;
        return new Ellipse (axis1, axis2);
        //break;
    }

    case 5:     {
        cout << "your choice N " << num << "is rectangle" << endl;
        int fside, sside;
        cout << "Set 1st side: ";
        cin >> fside;
        cout << "Set 2nd side: ";
        cin >> sside;
        return new Rectangle(fside, sside);
        //break;
    }

    default:     {
        cout << "Please try again" << endl;
        return nullptr;
        //break;
    }
    }
}

int main()
{
    int count;
    cout << "Enter count of figures: ";
    cin >> count;
    cout << endl << "go work with " << count << " figures" << endl;
    std::vector<Figure*> vFig;
        for (int i = 1; i <= count; i++) {
            vFig.push_back(setFigure(i));
    }

    //Figure** arr = new Figure*[count];
    //for (int i = 1; i <= count; i++) {
    //    arr[i-1] = setFigure(i);
    //}
    cout << "Results" << endl;
    for ( Figure* n:vFig) {
        cout << n->name << " ";
        cout << "P = " << n->get_per() << " ";
        cout << "S = " << n->get_sqr() << " ";
        cout << "P2 = " << n->get_per2() << endl;
    }
//    for (int i = 0; i < count; i++) {
//        cout << i+1 << "-" << arr[i]->name << " ";
//        cout << "P = " << arr[i]->get_per() << " ";
//        cout << "S = " << arr[i]->get_sqr() << " ";
//        cout << "P2 = " << arr[i]->get_per2() << endl;

    }
//    for (int i = 0; i < count; i++) {
//        delete arr[i];
//    }
//    delete[] arr;
//}
