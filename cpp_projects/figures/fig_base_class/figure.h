#ifndef FIGURE_H
#define FIGURE_H
//#pragma once
#include <string>
class Figure
{
protected:
    double per;
    double sqr;
    double p2;
public:
    Figure();
    virtual ~Figure();
    std::string name;
    virtual double get_per();
    virtual double get_sqr();
    virtual double get_per2();
};

#endif // FIGURE_H
