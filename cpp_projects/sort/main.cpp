/*// Example program
#include <iostream>
#include <string>
#include <iomanip>
#include <ctime>

void sort(int* arrPtr, int amt)
{
    std::cout << "call sort" << "!\n";
    int temp;
    bool exit = false;
    while (!exit) {
        exit=true;
        for (int i=0; i < amt-1; i++) {
            if(arrPtr[i+1] < arrPtr[i]) {
                temp = arrPtr[i];
                arrPtr[i] = arrPtr[i+1];
                arrPtr[i+1]=temp;
                exit = false;
            }
        }
    }
    for (int i=0; i<amt; i++)
         std::cout << arrPtr[i] << std::endl;
}

int main()
{
    int size;
    std::cout << "Enter size of array" << std::endl;
    std::cin >> size;
    int *arr= new int [size];
    std::cout << "Your array elements is ";
    for (int i = 0; i < size; i++) {
        arr[i] = rand() % 10;
        std::cout << arr[i]<< std::endl;
    }
    //int arr[] = {9, 3, 6, 5};
    sort(arr, size);
    delete[] arr;
}*/
// Example program
#include <iostream>
#include <string>
#include <array>

void sort(int arr[], int N) {
    std::cout << "!\n" << "Call sort" << "!\n";

    bool exit = false;
    int temp = 1;
    while (!exit) {
        exit = true;
        for (int i=0; i<N-1; i++) {
            if (arr[i+1]<arr[i]) {
                temp = arr [i];
                arr[i] = arr[i+1];
                arr[i+1] = temp;
                exit = false;
            }
        }
    }
    for (int i = 0; i<N; i++) {
         std::cout << arr[i] << " ";
    }
}

int main()
{
    int arr[] = {4, 2, 7, 5, -9};
    std::cout << "Sort array = ";
    for (int i = 0; i<sizeof((arr))/sizeof((arr[0])); i++)
            std::cout << arr[i] << " ";
    sort(arr, 5);
}

