package MainPack;

import java.util.regex.Pattern;
import java.awt.datatransfer.Clipboard;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.sun.jna.platform.win32.WinNT.HANDLE;

public class TestClass {
	  private WebDriver driverMail;
	  private WebDriver driverReg;
	  private WebDriver driverCheck;
	  private String baseUrlMail;
	  private String baseUrlReg;
	  private String urlReg;
	  private String wrongMail;
	  private String passCombo;
	  private String passLit;
	  private String passSym;
	  private StringBuffer verificationErrors = new StringBuffer();
	  private Random random;
	 
 
	 	  
  public void bookOnMailSource() {
	  try {
		  driverMail.get(baseUrlMail);
		  driverMail.findElement(By.linkText("Start Here")).click();
	   	  driverMail.findElement(By.id("inbox-name")).click();
	   	  
	  }
	  catch (ArithmeticException e) {
			 System.out.println("Registration email is fail");
	  }
  	    
  }
  
  public void bookOnSource(String pass, boolean key) {
	  try {
		    driverReg.get(baseUrlReg);
			driverReg.findElement(By.linkText("������������������")).click();
			driverReg.findElement(By.id("name")).clear();
			driverReg.findElement(By.id("name")).sendKeys("testSel" + Integer.toString(random.nextInt(1000)));
			driverReg.findElement(By.id("email")).clear();
			if (key) 
				driverReg.findElement(By.id("email")).sendKeys(Keys.CONTROL + "v");
			else driverReg.findElement(By.id("email")).sendKeys("ksenia-simka@mail.ru");
			driverReg.findElement(By.id("password")).clear();
		    driverReg.findElement(By.id("password")).sendKeys(pass);
		    
		    if (!driverReg.findElement(By.id("accept-tos")).isSelected()) {
		        driverReg.findElement(By.id("accept-tos")).click();
		    };
		    driverReg.findElement(By.id("signup")).click();
		    
	  }
	  catch (ArithmeticException e) {
		  System.out.println("Registration accaunt is fail");
	  }
  }
  
  public void logInSource(String pass) {
	  try {
	    	driverMail.findElement(By.linkText("��������� �����")).click();
			driverCheck.get(urlReg);
	    	driverCheck.findElement(By.id("user")).clear();
	    	driverCheck.findElement(By.id("user")).sendKeys(Keys.CONTROL + "v");
	    	driverCheck.findElement(By.id("password")).clear();
	    	driverCheck.findElement(By.id("password")).sendKeys(pass);
	    	driverCheck.findElement(By.id("login")).click();
	    	driverCheck.findElement(By.className("header-logo-default")).click();
	    	System.out.println("Check success");
	    }
	    
		 catch (ArithmeticException e) {
			 String text = driverCheck.findElement(By.className("error-message")).getText();
			 System.out.println(text);
			 tearDown();
		}
  }
  
  @Before
  public void setUp() /*throws Exception*/ 
  {
	  System.setProperty("webdriver.gecko.driver", "d:\\Soft\\geckodriver.exe");
	  driverMail = new FirefoxDriver();
	  driverReg = new FirefoxDriver();
	  driverCheck = new FirefoxDriver();
	  baseUrlMail = "https://mytemp.email/";
	  baseUrlReg = "https://trello.com/";
	  urlReg = baseUrlReg + "login?";
	  wrongMail = "1234@mail.com";
	  passCombo = "1__pass__5";
	  passLit = "qwertyui";
	  passSym = "_=+-`_)*";
	  driverMail.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  driverReg.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  random = new Random();
  }
  
  /*  @Test
  //���� �� �������� �������� ����������� �� trello.com
  public void test() throws Exception 
  {	  
	bookOnMailSource();
    bookOnSource(passCombo,true);
	Thread.sleep(10000);
    driverMail.findElement(By.xpath("//body[@id='app']/div/md-content/div/div/md-list/md-list-item/a/div/span")).click();
    driverMail.switchTo().frame(driverMail.findElement(By.id("body-html-iframe-content")));
    logInSource(passCombo);
 }
   */
  @Test
  public void testPos1() throws Exception {
	    bookOnMailSource();
	    bookOnSource(passCombo,true);
		Thread.sleep(5000);
		try {
			isElementPresent(By.linkText("����� ���������� �� �����"),driverReg);
			System.out.println("Registration accaunt success");
		}
		catch (ArithmeticException e) {
			  System.out.println("Registration accaunt is fail");
		  }
	    
  }
   
   @Test
  public void testPos2() throws Exception {
	    bookOnMailSource();
	    bookOnSource(passLit, true);
		Thread.sleep(5000);
		try {
			isElementPresent(By.linkText("����� ���������� �� �����"),driverReg);
			System.out.println("Registration accaunt success");
		}
		catch (ArithmeticException e) {
			  System.out.println("Registration accaunt is fail");
		  }
  }
   
   @Test
   public void testPos3() throws Exception {
 	    bookOnMailSource();
 	    bookOnSource(passSym, true);
 		Thread.sleep(5000);
 		try {
 			isElementPresent(By.linkText("����� ���������� �� �����"),driverReg);
 			System.out.println("Registration accaunt success");
 		}
 		catch (ArithmeticException e) {
 			  System.out.println("Registration accaunt is fail");
 		  }
   }
  
   @Test
   public void testNEg2() throws Exception {
 	    bookOnMailSource();
 	    bookOnSource("1", true);
 		Thread.sleep(5000);
 		try {
 			isElementPresent(By.linkText("����� ���������� �� �����"),driverReg);
 			System.out.println("Registration accaunt success");
 		}
 		catch (ArithmeticException e) {
 			  System.out.println("Registration accaunt is fail");
 		  }
   }
   
  @Test
  public void testNeg1() throws Exception {
	  bookOnMailSource();
	    bookOnSource(passCombo, false);
		Thread.sleep(5000);
		try {
			isElementPresent(By.linkText("����� ���������� �� �����"),driverReg);
			System.out.println("Registration accaunt success");
		}
		catch (ArithmeticException e) {
			  System.out.println("Registration accaunt is fail");
		  }
 }
	   
  @After
  public void tearDown() /*throws Exception*/ 
  {
	driverMail.quit();
	driverReg.quit();
	driverCheck.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by, WebDriver driver) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  /*private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }*/

  /*private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }*/

}
